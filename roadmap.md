# Technical TODO
Use java records for DTO objects
https://sigpwned.com/2024/09/22/generating-java-record-classes-with-jackson-annotations-to-map-json-using-chatgpt/

# Roadmap

## Cel

System Developer ma być systemem wspomagającym w modelowaniu i budowaniu systemu.

System jest rozumiany jako zbiór współpracujących ze sobą aplikacji (przez API).
System udostępnia swoją funkcjonalność na zewnątrz za pośrednictwem gui lub API.
Przy realizacji funkcjonalności mogą być użyte również usługi dostarczane przez aplikacje zewnętrzne (przez API).


Podstawowym elementem opisu systemu jest jednak opis jego funkcjonalności, gdyż stanowi ona uzasadnienie dla jego elementów (aplikacje, API).
Zasadniczo powinny być następujące opisy funkcjonalności o różnym poziomie abstrakcji
1. requirements: ogólny, abstrahujący od detali,
2. use-cases: konkretny, uwzględniający detale implementacyjne.
3. user stories: konkretny, uwzględniający detale biznesowe, a nie implementacyjne
4. architektura: diagram sekwencji 
5. słownik biznesowy: opis pojęć biznesowych, które powinny być znane ludziom z IT, aby dobrze rozumieli przekaz od ludzi z biznesu


## Założenia ogólne

- zapis modelu w plikach tekstowych w strukturze katalogów i trzymanie tego w git
  - wersjonowanie out-of-the-box
  - w razie problemów mergowanie ręczne plików tekstowych
  - duża wartość nawet bez aplikacji wspomagającej (ciągle stanowi to porządny opis)

- parametry integracyjne z zewnętrznymi aplikacjami (w tym DB)
  - nazwy parametrów dla endpointów i credentiali zewnętrznych aplikacji

- integracja z git
  - zarządzanie wersjami (tworzenie i mergowanie branchy - dla modelu systemu, jak i dla aplikacji)
  - referencje do repo aplikacji,
  - tworzenie repo dla nowej aplikacji,

- integracja z jenkins
  - tworzenie job'a dla aplikacji (ci/cd)
  - monitorowanie statusów (dla brancha wersji - chronionego)

- integracja z k8s
  - monitorowanie aplikacji
  - może tu cd, a nie w jenkins?

- referencje do elementów

  Preferowane rozwiązanie to referencje po ścieżce.
  - po ścieżce (w systemie plików i wewnątrz yaml/json, lub xml)
    - zmiana nazwy, która zmienia ścieżkę powinna zmienić też referencję
      Jednak specyfikacje będą edytowane przez edytor tekstowy.
      I o ile łatwo wykryć, że element o danej ścieżce zniknął, to trudniej jest zgadnąć jaki element go zastąpił...
  - po id
    - należy dokleić UUID do elementu (to może nie być możliwe przy sztywnych schematach)
    - refactor nazw jest bezbolesny, bo UUID się nie zmienia
    - referencje nie są czytelne bez aplikacji wspomagającej (odnoszą się do UUID)


## Struktura opisu systemu

```
system
├── apis (M1)
│   ├── external
│   │   └── api-ex-X
│   ├── private
│   │   └── api-pr-X
│   └── public
│       └── api-pu-X
│
├── apps (M2)
│   └── app-X
│
├── gui (M3)
│   └── group-X
│       └── sub-group-Y
│           └── screen-X-Y-Z
│
├── use-cases (M4)
│   └── group-X
│       └── ux-X-Y
│
├── use-stories (M5)
│
└── requirements (M6)
```

## API
API może być synchroniczne oraz asynchroniczne.
Obsługa standardów:
- OpenAPI
- WSDL
- asynchroniczne: schemat komunikatu (XSD/JSON schema) + nazwa kolejki

### Dostępność
 - public: używane przez zewnętrzne aplikacje, nad którymi nie ma kontroli
 - external: implementowane przez zewnętrzne aplikacje, a używane w systemie
 - private: używane tylko wewnątrz systemu

### Opis operacji

#### Opis minimalistyczny
Powinien być możliwy opis (draft architekta), gdzie będzie można określić, z jakich API operacja będzie korzystać.

#### Opis rozszerzony
Opis operacji powinien być w formie przypadku użycia. Opis ten będzie "includowany" do przypadku użycia, który będzie odwoływał się do tej operacji.

### Parametry integracyjne
Aby używać API, to potrzebny jest co najmniej adres, pod którym API jest wystawiane. Często są potrzebne również inne wartości, które są specyficzne dla instancji aplikacji implementującej API.

Dotyczy to w szczególności API wystawiane przez aplikacje zewnętrzne.

## Aplikacje
Aplikacje GUI lub backendowe.

Informacje o aplikacji
 - link do repozytorium kodów
 - skrypty instalacji (k8s, DB, skrypty do uruchomienia w zewnętrznych aplikacjach)
 - lista implementowanych API
   - generowanie kodu obsługi operacji
 - lista używanych API/aplikacji
   - ustalana na podstawie opisów operacji
   - generowanie kodu klienta wywoływania operacji

Aplikacja na "klik"
- utworzenie repozytorium git
- generowanie szkieletu aplikacji
- utworzenie job'a jenkins


## use cases

Precyzyjny opis działania systemu, który odnosi się do:
 - ekranów i ich elementów
 - operacji API ze wskazaniem jak z niego korzystać
   - jak wypełniać komunikat wejściowy,
   - jak korzystać z komunikatu wyjściowego
 - w danym kontekście wiadomo, które dane są dostępne (z ekranów, parametrów wejściowych operacji), i system powinien wspierać w specyfikowaniu jak tworzyć komunikat wejściowy wywołania operacji.

## requirements

Nieformalne wymagania biznesowe (user story) i ogólne wytyczne odnośnie do bezpieczeństwa i wydajności.

Wymagania biznesowe powinny mieć status wytwórczy (konfiguracja możliwych statusów)
 - przygotowanie wymagania
 - przygotowanie rozwiązania (architektura / analiza systemowa / development / test)
 - gotowe

Jednemu wymaganiu może odpowiadać wiele przypadków użyciu i odwrotnie jednemu przypadku użycia może odpowiadać wiele wymagań.


## Milestones

### M0 (realizacja w miarę potrzeb)
- Środowisko
  - git/keycloak/jenkins
- SSO

### M1 - Dokumentacja API
- Dodawanie i aktualizowanie API przez upload plików OpenAPI/wsdl
- Przeglądanie OpenAPI (widok operacji i struktury komunikatów)
- Przeglądanie WSDL
- Edycja OpenAPI (operacji i struktury komunikatów)

### M2 - Dokumentacja aplikacji

### M3 - Dokumentacja GUI

### M4 - Dokumentacja wymagań (UseCase'ów)

### M5 - Dokumentacja wymagań (nieszczegółowych)
