import org.springframework.boot.gradle.tasks.run.BootRun
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator
import java.time.LocalDate

/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

val commonsIoVersion: String by project
val lombokVersion: String by project
val spockVersion: String by project
val snakeyamlVersion: String by project
val jsonVersion: String by project

plugins {
    id("org.springframework.boot") version "2.7.18"
    id("io.spring.dependency-management") version "1.1.6"
    id("java")
    id("groovy")
    id("pl.metaprogramming.codegen") version "2.0.0"
}

java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.zalando:logbook-spring-boot-starter:2.14.0")
    implementation("com.google.code.findbugs:findbugs:3.0.1")
    implementation("commons-io:commons-io:$commonsIoVersion")
    implementation("org.json:json:$jsonVersion")
    implementation("org.yaml:snakeyaml:$snakeyamlVersion")
    compileOnly("org.projectlombok:lombok:$lombokVersion")
    annotationProcessor("org.projectlombok:lombok:$lombokVersion")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.spockframework:spock-spring:$spockVersion")
}

tasks.register<Copy>("copyTestApis") {
    group = "build"
    description = "Copy test api to local workspace directory"
    into("out/apis")
    from("src/test/resources/apis")
    eachFile {
        if (relativePath.getFile(destinationDir).exists()) {
            exclude()
        }
    }
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<BootRun> {
    dependsOn("copyTestApis")
}

codegen {
    params[JavaParams::class].licenceHeader = """Copyright (c) ${LocalDate.now().year} Dawid Walczak.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

    generate(SpringCommonsGenerator()) {
        rootPackage = "pl.metaprogramming.sd"
        typeOfCode(toc.SERIALIZATION_UTILS, toc.ENUM_VALUE_INTERFACE).forEach {
            it.packageName.tail = "utils"
        }
    }
    generate(SpringRestServiceGenerator()) {
        model = restApi(file("sd-api.yaml").readText())
        rootPackage = "pl.metaprogramming.sd"
    }
}