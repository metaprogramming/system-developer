/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.out

import pl.metaprogramming.sd.ports.in.rest.dtos.DataPropertyModelDto
import pl.metaprogramming.sd.ports.in.rest.dtos.DataSchemaModelDto
import pl.metaprogramming.sd.ports.in.rest.dtos.HttpParameterModelDto
import pl.metaprogramming.sd.ports.in.rest.dtos.HttpPayloadModelDto
import pl.metaprogramming.sd.ports.in.rest.dtos.HttpResponseModelDto
import spock.lang.Specification

import java.util.stream.Collectors
import java.util.stream.Stream

class OpenApiParserSpec extends Specification {

    def "should parse OpenApi 3.0 yaml - example-api.yaml"() {
        given:
        def path = "internal/example-api.yaml"
        def file = apiFile(path)
        def parser = new OpenApiParser(file.toPath(), "internal/")
        when:
        def modelDto = parser.parse()
        then:
        modelDto
        modelDto.kind == 'OAS3'
        modelDto.description == "Example API\nAPI description"

        when:
        def operations = modelDto.operations
        then:
        operations.size() == 13
        operations.collect { "${it.httpMethod} ${it.httpPath} - ${it.path}" } == [
                "GET /echo - ${path}#/paths/`/echo`/get",
                "POST /echo - ${path}#/paths/`/echo`/post",
                "POST /echo-arrays - ${path}#/paths/`/echo-arrays`/post",
                "GET /echo-date-array - ${path}#/paths/`/echo-date-array`/get",
                "POST /echo-defaults - ${path}#/paths/`/echo-defaults`/post",
                "GET /echo-empty - ${path}#/paths/`/echo-empty`/get",
                "GET /echo-error - ${path}#/paths/`/echo-error`/get",
                "DELETE /echo-file/{id} - ${path}#/paths/`/echo-file/{id}`/delete",
                "GET /echo-file/{id} - ${path}#/paths/`/echo-file/{id}`/get",
                "POST /echo-file/{id} - ${path}#/paths/`/echo-file/{id}`/post",
                "POST /echo-file/{id}/form - ${path}#/paths/`/echo-file/{id}/form`/post",
                "POST /echo-multi-content - ${path}#/paths/`/echo-multi-content`/post",
                "POST /skipped - ${path}#/paths/`/skipped`/post",
        ]

        modelDto.parameters.size() == 2
        modelDto.parameters.collect { toString(it) } == [
                "internal/example-api.yaml#/components/parameters/authorizationParam ; Authorization ; string ; HEADER",
                "internal/example-api.yaml#/components/parameters/correlationIdParam ; X-Correlation-ID ; string ; HEADER ; required"
        ]

        modelDto.schemas.size() == 10
        modelDto.schemas.collect { toString(it) } == [
                "internal/example-api.yaml#/components/schemas/UnusedEnum ; UnusedEnum ; enum",
                "internal/example-api.yaml#/components/schemas/UnusedObject ; UnusedObject ; object",
                "internal/example-api.yaml#/components/schemas/ObjectReferredOnlyBySkippedOperation ; ObjectReferredOnlyBySkippedOperation ; object",
                "internal/example-api.yaml#/components/schemas/ExtendedObject ; ExtendedObject ; object",
                "internal/example-api.yaml#/components/schemas/EchoBody ; EchoBody ; object",
                "internal/example-api.yaml#/components/schemas/EchoArraysBody ; EchoArraysBody ; object",
                "internal/example-api.yaml#/components/schemas/echo_defaults_body ; echo_defaults_body ; object",
                "internal/example-api.yaml#/components/schemas/DefaultEnum ; DefaultEnum ; enum",
                "internal/example-api.yaml#/components/schemas/EchoGetBody ; EchoGetBody ; object",
                "internal/example-api.yaml#/components/schemas/MultiContentBody ; MultiContentBody ; object"
        ]

        when:
        def echoPost = operations.find { it.path == "${path}#/paths/`/echo`/post" }
        echoPost.responseBody.collect { toString(it) }.flatten().forEach {
            println("'$it',")
        }
        then:
        echoPost.parameters.collect { toString(it) } == [
                "$path#/components/parameters/authorizationParam",
                "$path#/components/parameters/correlationIdParam",
                "internal/example-api-deps.yml#/components/parameters/timestampParam",
                "Inline-Header-Param ; string ; HEADER"
        ]
        echoPost.requestBody.collect { toString(it) } == [
                'application/json ; $ref:internal/example-api.yaml#/components/schemas/EchoBody',
        ]
        echoPost.requestBody.contentType == ['application/json']
        toString(echoPost.requestBody[0].type.type) == '$ref:internal/example-api.yaml#/components/schemas/EchoBody'
        echoPost.responseBody.collect { "${it.status} ; ${it.contents?.get(0)?.type?.type} ; ${it.description}".toString() } == [
                '200 ; $ref:internal/example-api.yaml#/components/schemas/EchoBody ; request body',
                'default ; $ref:internal/example-api-deps.yml#/components/schemas/ErrorDescription ; Error']

        when:
        def echoBody = modelDto.schemas.find { it.code == 'EchoBody' }
        then:
        echoBody.type.properties.collect { toString(it) } == [
                "prop_int ; int32",
                "prop_int_second ; int32",
                "prop_int_required ; int64 ; required",
                "prop_float ; float",
                "prop_double ; double",
                "prop_amount ; string",
                "prop_amount_number ; amount",
                "prop_string ; string",
                "prop_string_pattern ; string",
                "prop_default ; string",
                "prop_date ; date",
                "prop_date_second ; date",
                "prop_date_time ; date-time",
                "prop_base64 ; byte",
                "prop_boolean ; boolean",
                "prop_object ; \$ref:internal/example-api-deps.yml#/components/schemas/SimpleObject",
                "prop_object_any ; object",
                "prop_object_extended ; \$ref:internal/example-api.yaml#/components/schemas/ExtendedObject",
                "prop_enum_reusable ; \$ref:internal/example-api-deps.yml#/components/schemas/ReusableEnum",
                "prop_enum ; enum"
        ]
    }

    def "should parse swagger 2.0 json file - petstore"() {
        given:
        def path = "external/petstore/petstore-oas2.json"
        def file = apiFile(path)
        def parser = new OpenApiParser(file.toPath(), "external/petstore/")

        when:
        def modelDto = parser.parse()
        then:
        modelDto
        modelDto.kind == 'OAS2'
        modelDto.path == path
        modelDto.description == "Swagger Petstore\nThis is a sample server Petstore server.  You can find out more about Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, you can use the api key `special-key` to test the authorization filters."

        when:
        def operations = modelDto.operations
        then:
        operations.size() == 20
        operations.collect { "${it.httpMethod} ${it.httpPath} - ${it.path}" } == [
                "POST /pet - ${path}#/paths/`/pet`/post",
                "PUT /pet - ${path}#/paths/`/pet`/put",
                "GET /pet/findByStatus - ${path}#/paths/`/pet/findByStatus`/get",
                "GET /pet/findByTags - ${path}#/paths/`/pet/findByTags`/get",
                "DELETE /pet/{petId} - ${path}#/paths/`/pet/{petId}`/delete",
                "GET /pet/{petId} - ${path}#/paths/`/pet/{petId}`/get",
                "POST /pet/{petId} - ${path}#/paths/`/pet/{petId}`/post",
                "POST /pet/{petId}/uploadImage - ${path}#/paths/`/pet/{petId}/uploadImage`/post",
                "GET /store/inventory - ${path}#/paths/`/store/inventory`/get",
                "POST /store/order - ${path}#/paths/`/store/order`/post",
                "DELETE /store/order/{orderId} - ${path}#/paths/`/store/order/{orderId}`/delete",
                "GET /store/order/{orderId} - ${path}#/paths/`/store/order/{orderId}`/get",
                "POST /user - ${path}#/paths/`/user`/post",
                "POST /user/createWithArray - ${path}#/paths/`/user/createWithArray`/post",
                "POST /user/createWithList - ${path}#/paths/`/user/createWithList`/post",
                "GET /user/login - ${path}#/paths/`/user/login`/get",
                "GET /user/logout - ${path}#/paths/`/user/logout`/get",
                "DELETE /user/{username} - ${path}#/paths/`/user/{username}`/delete",
                "GET /user/{username} - ${path}#/paths/`/user/{username}`/get",
                "PUT /user/{username} - ${path}#/paths/`/user/{username}`/put",
        ]

        modelDto.parameters.isEmpty()

        modelDto.schemas.size() == 6
        modelDto.schemas.collect { toString(it) } == [
                "external/petstore/petstore-oas2.json#/definitions/Order ; Order ; object",
                "external/petstore/petstore-oas2.json#/definitions/Category ; Category ; object",
                "external/petstore/petstore-oas2.json#/definitions/User ; User ; object",
                "external/petstore/petstore-oas2.json#/definitions/Tag ; Tag ; object",
                "external/petstore/petstore-oas2.json#/definitions/Pet ; Pet ; object",
                "external/petstore/petstore-oas2.json#/definitions/ApiResponse ; ApiResponse ; object"
        ]
        when:
        def findPetsByStatus = operations.find { it.path == "${path}#/paths/`/pet/findByStatus`/get" }
        def statusParam = findPetsByStatus.parameters.find { it.name == 'status' }
        then:
        statusParam != null
        statusParam.isRequired
        statusParam.type.type == "array:enum"
        statusParam.kind == 'QUERY'
        findPetsByStatus.requestBody == null
        findPetsByStatus.responseBody.collect { toString(it) }.flatten() == [
                '200 ; successful operation ; application/json ; array:$ref:external/petstore/petstore-oas2.json#/definitions/Pet',
                '200 ; successful operation ; application/xml ; array:$ref:external/petstore/petstore-oas2.json#/definitions/Pet',
                '400 ; Invalid status value',
        ]

        when:
        def postPet = operations.find { it.path == "${path}#/paths/`/pet`/post" }
        then:
        postPet.parameters.empty
        postPet.requestBody.collect {toString(it) } == [
                'application/json ; $ref:external/petstore/petstore-oas2.json#/definitions/Pet',
                'application/xml ; $ref:external/petstore/petstore-oas2.json#/definitions/Pet',
        ]
        postPet.responseBody.collect { toString(it) }.flatten() == [
                '405 ; Invalid input'
        ]

        when:
        def postPetFormData = operations.find { it.path == "${path}#/paths/`/pet/{petId}`/post" }
        then:
        postPetFormData.parameters.collect { toString(it) } == [
                'petId ; int64 ; PATH ; required',
                'name ; string ; FORMDATA',
                'status ; string ; FORMDATA']
        !postPetFormData.requestBody
        postPetFormData.responseBody.collect { toString(it) }.flatten() == [
                '405 ; Invalid input'
        ]

        when:
        def login = operations.find { it.path == "${path}#/paths/`/user/login`/get" }
        then:
        !login.requestBody
        login.responseBody.collect { toString(it) }.flatten() == [
                '200 ; successful operation ; application/json ; string ; headers:[X-Rate-Limit ; int32, X-Expires-After ; date-time]',
                '200 ; successful operation ; application/xml ; string ; headers:[X-Rate-Limit ; int32, X-Expires-After ; date-time]',
                '400 ; Invalid username/password supplied',
        ]
    }

    def "should parse swagger 2.0 yaml file - PolishAPI"() {
        given:
        def path = "public/PolishAPI-ver3_0.yaml"
        def file = apiFile(path)
        def parser = new OpenApiParser(file.toPath(), "public/")

        when:
        def modelDto = parser.parse()
        then:
        modelDto
        modelDto.kind == 'OAS2'
        modelDto.path == path
        modelDto.description == "Polish API\nSpecyfikacja interfejsu dla usług świadczonych przez ASPSP w oparciu o dostęp do rachunków płatniczych. Opracowane przez Związek Banków Polskich i podmioty współpracujące / Interface specification for services provided by ASPSP based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates"

        when:
        def operations = modelDto.operations
        then:
        operations.size() == 27
        operations.collect { "${it.httpMethod} ${it.httpPath} - ${it.path}" } == [
                "POST /v3_0.1/accounts/v3_0.1/deleteConsent - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/deleteConsent`/post",
                "POST /v3_0.1/accounts/v3_0.1/getAccount - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getAccount`/post",
                "POST /v3_0.1/accounts/v3_0.1/getAccounts - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getAccounts`/post",
                "POST /v3_0.1/accounts/v3_0.1/getHolds - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getHolds`/post",
                "POST /v3_0.1/accounts/v3_0.1/getTransactionDetail - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getTransactionDetail`/post",
                "POST /v3_0.1/accounts/v3_0.1/getTransactionsCancelled - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getTransactionsCancelled`/post",
                "POST /v3_0.1/accounts/v3_0.1/getTransactionsDone - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getTransactionsDone`/post",
                "POST /v3_0.1/accounts/v3_0.1/getTransactionsPending - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getTransactionsPending`/post",
                "POST /v3_0.1/accounts/v3_0.1/getTransactionsRejected - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getTransactionsRejected`/post",
                "POST /v3_0.1/accounts/v3_0.1/getTransactionsScheduled - ${path}#/paths/`/v3_0.1/accounts/v3_0.1/getTransactionsScheduled`/post",
                "POST /v3_0.1/auth/v3_0.1/authorize - ${path}#/paths/`/v3_0.1/auth/v3_0.1/authorize`/post",
                "POST /v3_0.1/auth/v3_0.1/authorizeExt - ${path}#/paths/`/v3_0.1/auth/v3_0.1/authorizeExt`/post",
                "POST /v3_0.1/auth/v3_0.1/register - ${path}#/paths/`/v3_0.1/auth/v3_0.1/register`/post",
                "POST /v3_0.1/auth/v3_0.1/token - ${path}#/paths/`/v3_0.1/auth/v3_0.1/token`/post",
                "POST /v3_0.1/confirmation/v3_0.1/getConfirmationOfFunds - ${path}#/paths/`/v3_0.1/confirmation/v3_0.1/getConfirmationOfFunds`/post",
                "POST /v3_0.1/payments/v3_0.1/EEA - ${path}#/paths/`/v3_0.1/payments/v3_0.1/EEA`/post",
                "POST /v3_0.1/payments/v3_0.1/bundle - ${path}#/paths/`/v3_0.1/payments/v3_0.1/bundle`/post",
                "POST /v3_0.1/payments/v3_0.1/cancelPayments - ${path}#/paths/`/v3_0.1/payments/v3_0.1/cancelPayments`/post",
                "POST /v3_0.1/payments/v3_0.1/cancelRecurringPayment - ${path}#/paths/`/v3_0.1/payments/v3_0.1/cancelRecurringPayment`/post",
                "POST /v3_0.1/payments/v3_0.1/domestic - ${path}#/paths/`/v3_0.1/payments/v3_0.1/domestic`/post",
                "POST /v3_0.1/payments/v3_0.1/getBundle - ${path}#/paths/`/v3_0.1/payments/v3_0.1/getBundle`/post",
                "POST /v3_0.1/payments/v3_0.1/getMultiplePayments - ${path}#/paths/`/v3_0.1/payments/v3_0.1/getMultiplePayments`/post",
                "POST /v3_0.1/payments/v3_0.1/getPayment - ${path}#/paths/`/v3_0.1/payments/v3_0.1/getPayment`/post",
                "POST /v3_0.1/payments/v3_0.1/getRecurringPayment - ${path}#/paths/`/v3_0.1/payments/v3_0.1/getRecurringPayment`/post",
                "POST /v3_0.1/payments/v3_0.1/nonEEA - ${path}#/paths/`/v3_0.1/payments/v3_0.1/nonEEA`/post",
                "POST /v3_0.1/payments/v3_0.1/recurring - ${path}#/paths/`/v3_0.1/payments/v3_0.1/recurring`/post",
                "POST /v3_0.1/payments/v3_0.1/tax - ${path}#/paths/`/v3_0.1/payments/v3_0.1/tax`/post",
        ]

        modelDto.parameters.size() == 6
        modelDto.parameters.collect { toString(it) } == [
                "public/PolishAPI-ver3_0.yaml#/parameters/authorizationParam ; Authorization ; string ; HEADER ; required",
                "public/PolishAPI-ver3_0.yaml#/parameters/acceptEncodingParam ; Accept-Encoding ; enum ; HEADER ; required",
                "public/PolishAPI-ver3_0.yaml#/parameters/acceptLanguageParam ; Accept-Language ; string ; HEADER ; required",
                "public/PolishAPI-ver3_0.yaml#/parameters/acceptCharsetParam ; Accept-Charset ; enum ; HEADER ; required",
                "public/PolishAPI-ver3_0.yaml#/parameters/xjwsSignatureParam ; X-JWS-SIGNATURE ; string ; HEADER ; required",
                "public/PolishAPI-ver3_0.yaml#/parameters/xRequestIdParam ; X-REQUEST-ID ; string ; HEADER ; required",
        ]

        modelDto.schemas.size() == 123

        when:
        def deleteConsent = operations.find { it.path == "${path}#/paths/`/v3_0.1/accounts/v3_0.1/deleteConsent`/post" }
        then:
        deleteConsent.requestBody.collect { toString(it) } == [
                'application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/DeleteConsentRequest',
        ]
        deleteConsent.requestBody.contentType == ['application/json']
        deleteConsent.requestBody.type.type == ['$ref:public/PolishAPI-ver3_0.yaml#/definitions/DeleteConsentRequest']
        deleteConsent.parameters.collect { toString(it) } == [
                'public/PolishAPI-ver3_0.yaml#/parameters/acceptEncodingParam',
                'public/PolishAPI-ver3_0.yaml#/parameters/acceptLanguageParam',
                'public/PolishAPI-ver3_0.yaml#/parameters/acceptCharsetParam',
                'public/PolishAPI-ver3_0.yaml#/parameters/xjwsSignatureParam',
                'public/PolishAPI-ver3_0.yaml#/parameters/xRequestIdParam'
        ]
        deleteConsent.responseBody.collect { toString(it) }.flatten() == [
                '204 ; Powodzenie / Success ; headers:[Content-Encoding ; string]',
                '400 ; Błędne żądanie / Bad request ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '401 ; Nieautoryzowany dostęp / Unauthorized ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '403 ; Zabroniony / Forbidden ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '405 ; Metoda niedozwolona / Method Not Allowed ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '406 ; Niedozwolone / Not Acceptable ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '415 ; Nieznany sposób żądania / Unsupported Media Type ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '422 ; Jednostka nieprzetwarzalna / Unprocessable entity ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '500 ; Wewnętrzny błąd serwera / Internal Server Error ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '501 ; Nie zaimplementowano / Not Implemented ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
                '503 ; Usługa niedostępna / Service Unavailable ; application/json ; $ref:public/PolishAPI-ver3_0.yaml#/definitions/Error',
        ]
    }

    File apiFile(String path) {
        new File("src/test/resources/apis/$path")
    }

    String toString(HttpParameterModelDto param) {
        toString(param.path, param.name, param.type?.type, param.kind, param.isRequired ? "required" : null)
    }

    String toString(DataSchemaModelDto schema) {
        toString(schema.path, schema.code, schema.type?.type)
    }

    String toString(DataPropertyModelDto prop) {
        toString(prop.name, prop.type?.type, prop.isRequired ? "required" : null)
    }

    String toString(HttpPayloadModelDto payload) {
        toString(payload.contentType, payload?.type?.type)
    }

    List<String> toString(HttpResponseModelDto res) {
        def headers = res.headers?.collect { toString(it) }
        def headersPart = headers ? 'headers:' + headers.toString() : null
        if (!res.contents) {
            [toString(res.status, res.description, headersPart)]
        } else {
            res.contents.collect {
                toString(res.status, res.description, it.contentType, it.type?.type, headersPart)
            }
        }
    }

    String toString(def ... parts) {
        Stream.of(parts).filter(Objects::nonNull).collect(Collectors.joining(' ; '))
    }
}
