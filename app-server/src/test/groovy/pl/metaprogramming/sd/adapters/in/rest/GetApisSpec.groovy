/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest

import org.springframework.test.context.TestPropertySource
import pl.metaprogramming.sd.RestSpecification
import pl.metaprogramming.sd.ports.in.rest.dtos.RestApiModelDto

import static org.springframework.http.HttpMethod.GET

@TestPropertySource(properties = ["workspaceDirectory=build/resources/test"])
class GetApisSpec extends RestSpecification {

    def "should get all apis"() {
        when:
        def result = call(GET, '/apis/all').exchange(List)
        def list = result.body as List<RestApiModelDto>
        then:
        result.statusCodeValue == 200
        list.size() == 7

        when:
        def exampleApi = list.find { it.path == 'internal/example-api.yaml' }
        def echoPost = exampleApi.operations.find { it.httpPath == '/echo' && it.httpMethod == 'POST' }
        then:
        echoPost
        echoPost.summary == null
        echoPost.description == null

        when:
        def summary = list.collect { "$it.path | $it.kind".toString() }
        then:
        summary == [
                'external/petstore/petstore-oas2.json | OAS2',
                'external/petstore/petstore-oas3.json | OAS3',
                'external/petstore/petstore-oas3.yaml | OAS3',
                'internal/example-api-deps.yml | OAS3',
                'internal/example-api.yaml | OAS3',
                'public/PolishAPI-CallBack-ver3_0.yaml | OAS2',
                'public/PolishAPI-ver3_0.yaml | OAS2'
        ]
    }
}
