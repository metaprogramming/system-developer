/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest

import org.springframework.http.ResponseEntity
import org.springframework.test.context.TestPropertySource
import pl.metaprogramming.sd.RestSpecification
import pl.metaprogramming.sd.adapters.out.FsUtils
import pl.metaprogramming.sd.adapters.out.ZipUtils
import pl.metaprogramming.sd.ports.in.rest.dtos.DirectoryDto
import pl.metaprogramming.sd.ports.in.rest.dtos.FileCopyDto
import spock.lang.Stepwise

import static org.springframework.http.HttpMethod.*

@Stepwise
@TestPropertySource(properties = ["workspaceDirectory=build/root"])
class FilesSpec extends RestSpecification {

    def setupSpec() {
        new File("build/root").deleteDir()
    }

    def "should return fixed directories"() {
        when:
        def result = getList()
        then:
        result.statusCodeValue == 200
        result.body.name == '/'
        findDir(result, 'apis/public')
        findDir(result, 'apis/private')
        findDir(result, 'apis/external')
        findDir(result, 'apps')
    }

    def "should create directory 'test-dir'"() {
        when:
        def result = put('test-dir/')
        then:
        result.statusCodeValue == 201
        findDir('test-dir')

    }

    def "should create file '/test-dir/file1.txt'"() {
        when:
        def result = put('/test-dir/file1.txt', TEST_FILE_BODY)
        then:
        result.statusCodeValue == 201
        findFile('test-dir/file1.txt')
    }

    def "should download file body '/test-dir/file1.txt'"() {
        expect:
        checkFile('/test-dir/file1.txt', TEST_FILE_BODY)
    }

    def "should update file body '/test-dir/file1.txt'"() {
        when:
        def updateResult = put('/test-dir/file1.txt', TEST_FILE_BODY2)
        then:
        updateResult.statusCodeValue == 201
        checkFile('/test-dir/file1.txt', TEST_FILE_BODY2)
    }

    def "should add file '/test-dir/file2.txt'"() {
        when:
        def putResult = put('/test-dir/file2.txt', TEST_FILE_BODY)
        then:
        putResult.statusCodeValue == 201
        findFile('test-dir/file2.txt')
    }

    def "should download zip'ed dir '/test-dir/"() {
        when:
        def outDir = mkdir('zip1')
        def zipFiles = unzip('/test-dir', outDir)
        then:
        zipFiles.dirs == null
        zipFiles.files == ['file1.txt', 'file2.txt']
        checkFile(outDir, 'file1.txt', TEST_FILE_BODY2)
        checkFile(outDir, 'file2.txt', TEST_FILE_BODY)
    }

    def "should upload zip to new location '/test-dir/duplicate/"() {
        // przesłanie zip do nowej lokalizacji
        given:
        def bytes = new File("build/download.zip").bytes
        def path = '/test-dir/duplicated/'
        when:
        def upload = call(PATCH, 'files')
                .queryParam('path', path)
                .headerParam("content-type", "application/zip")
                .body(bytes).exchange(Void)
        then:
        upload.statusCodeValue == 204

        when:
        def listResult = getList()
        then:
        findDir(listResult, 'test-dir')
        findFile(listResult, 'test-dir/file1.txt')
        findFile(listResult, 'test-dir/file2.txt')
        findDir(listResult, 'test-dir/duplicated')
        findFile(listResult, 'test-dir/duplicated/file1.txt')
        findFile(listResult, 'test-dir/duplicated/file2.txt')
        checkFile('/test-dir/file1.txt', TEST_FILE_BODY2)
        checkFile('/test-dir/file2.txt', TEST_FILE_BODY)
        checkFile('/test-dir/duplicated/file1.txt', TEST_FILE_BODY2)
        checkFile('/test-dir/duplicated/file2.txt', TEST_FILE_BODY)
    }

    def "should upload zip to existing location '/test-dir/"() {
        given:
        def path = '/test-dir'
        when:
        def outDir = mkdir('zip2')
        def zipFiles = unzip(path, outDir)
        then:
        zipFiles.files == ['file1.txt', 'file2.txt']
        zipFiles.dirs.name == ['duplicated']
        zipFiles.dirs[0].files == ['file1.txt', 'file2.txt']
        zipFiles.dirs[0].dirs == null

        when:
        assert new File(outDir, 'file2.txt').delete()
        setFile(outDir, 'file1.txt', TEST_FILE_BODY)
        setFile(outDir, 'duplicated/file2.txt', TEST_FILE_BODY2)
        def zipData = ZipUtils.zipDir(outDir)
        def upload = call(PATCH, 'files')
                .queryParam('path', path)
                .headerParam("content-type", "application/zip")
                .body(zipData).exchange(Void)
        then:
        upload.statusCodeValue == 204

        when:
        def listResult = getList()
        then:
        findDir(listResult, 'test-dir')
        findFile(listResult, 'test-dir/file1.txt')
        findFile(listResult, 'test-dir/file2.txt')
        findDir(listResult, 'test-dir/duplicated')
        findFile(listResult, 'test-dir/duplicated/file1.txt')
        findFile(listResult, 'test-dir/duplicated/file2.txt')
        checkFile('/test-dir/file1.txt', TEST_FILE_BODY)
        checkFile('/test-dir/file2.txt', TEST_FILE_BODY)
        checkFile('/test-dir/duplicated/file1.txt', TEST_FILE_BODY2)
        checkFile('/test-dir/duplicated/file2.txt', TEST_FILE_BODY2)
    }

    def "should delete file '/test-dir/file2.txt'"() {
        when:
        def deleteResult = delete('/test-dir/file2.txt')
        then:
        deleteResult.statusCodeValue == 204
        !findFile('test-dir/file2.txt')
    }

    def "should add and delete dir with file '/test-dir2/file.txt'"() {
        when:
        put('test-dir2/file.txt', TEST_FILE_BODY)
        then:
        findFile('test-dir2/file.txt')

        when:
        delete('test-dir2')
        def listResult = getList()
        then:
        !findFile(listResult, 'test-dir2/file.txt')
        !findDir(listResult, 'test-dir2')
    }

    def "should copy file"() {
        when:
        copyFile('test-dir/file1.txt', 'copied-file1.txt', false)
        then:
        findFile('copied-file1.txt')
    }

    def "should copy directory"() {
        when:
        copyFile('test-dir', 'test-dir-copied', false)
        def listResult = getList()
        then:
        findFile(listResult, 'test-dir-copied/file1.txt')
        findFile(listResult, 'test-dir-copied/duplicated/file2.txt')
    }

    def "should move file"() {
        when:
        copyFile('test-dir-copied/file1.txt', 'test-dir-copied/moved-file1.txt', true)
        def listResult = getList()
        then:
        !findFile(listResult, 'test-dir-copied/file1.txt')
        findFile(listResult, 'test-dir-copied/moved-file1.txt')
    }

    def "should move directory"() {
        when:
        copyFile('test-dir-copied', 'test-dir-moved', true)
        def listResult = getList()
        then:
        !findFile(listResult, 'test-dir-copied')
        findDir(listResult, 'test-dir-moved')
        findFile(listResult, 'test-dir-moved/duplicated/file2.txt')
    }

    private def setFile(File dir, String path, String text) {
        new File(dir, path).setText(text, 'UTF8')
    }

    private def unzip(String path, File outDir) {
        def download = getBinary(path)
        assert download.statusCodeValue == 200
        ZipUtils.unzip(download.body, outDir)
        FsUtils.listDirectory(new DirectoryDto(), outDir)
    }

    private def mkdir(String name) {
        def dir = new File("build/$name")
        dir.deleteDir()
        assert !dir.exists()
        dir
    }

    private def put(String path, String body = null) {
        call(PUT, 'files').queryParam('path', path).body(body).exchange(Void)
    }

    private boolean checkFile(String path, String expectedBody) {
        def result = getText(path)
        assert result.statusCodeValue == 200
        assert result.body == expectedBody
        true
    }

    private boolean checkFile(File dir, String path, String expectedBody) {
        assert new File(dir, path).text == expectedBody
        true
    }

    private def getText(String path) {
        call(GET, 'files').queryParam('path', path).exchange(String)
    }

    private def getBinary(String path) {
        call(GET, 'files').queryParam('path', path).exchange(byte[])
    }

    private def delete(String path) {
        call(DELETE, 'files').queryParam('path', path).exchange(Void)
    }

    private ResponseEntity<DirectoryDto> getList() {
        call(GET, 'files-list').exchange(DirectoryDto)
    }

    private def copyFile(String src, String dst, boolean isMove) {
        call(POST, "files-copy").body(new FileCopyDto(src: src, dst: dst, isMove: isMove)).exchange(Void)
    }


    private DirectoryDto findDir(String path) {
        find(getList().body, path) as DirectoryDto
    }

    private DirectoryDto findDir(ResponseEntity<DirectoryDto> response, String path) {
        find(response.body, path) as DirectoryDto
    }

    private boolean findFile(String path) {
        find(getList().body, path) instanceof String
    }

    private boolean findFile(ResponseEntity<DirectoryDto> response, String path) {
        find(response.body, path) instanceof String
    }

    private def find(DirectoryDto root, String path) {
        if (root == null) return null
        def idx = path.indexOf('/')
        if (idx > 0) {
            find(find(root, path.substring(0, idx)) as DirectoryDto, path.substring(idx + 1))
        } else {
            root.dirs.find { it.name == path } ?: root.files.find { it == path }
        }
    }

    static final String TEST_FILE_BODY = '''To jest plik z wieloma liniami:
Linia ąć
Linia żź
'''
    static final String TEST_FILE_BODY2 = 'To jest druga wersja pliku'
}
