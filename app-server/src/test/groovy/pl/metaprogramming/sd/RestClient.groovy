/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package pl.metaprogramming.sd

import org.springframework.http.HttpMethod
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

class RestClient {

    private final Map<String,Object> pathParams = new HashMap<>()
    private final MultiValueMap<String,String> headerParams = new LinkedMultiValueMap<>()
    private final MultiValueMap<String,Object> formDataParams = new LinkedMultiValueMap<>()
    private final MultiValueMap<String,String> queryParams = new LinkedMultiValueMap<>()

    private final HttpMethod method
    private final String path
    private final RestTemplate restTemplate
    private Object body

    RestClient(RestTemplate restTemplate, HttpMethod method, String path) {
        this.restTemplate = restTemplate
        this.method = method
        this.path = path
    }

    RestClient body(Object body) {
        this.body = body
        this
    }

    RestClient headerParam(String name, Object value) {
        if (value) headerParams.add(name, value.toString())
        this
    }

    RestClient queryParam(String name, Object value) {
        if (value) queryParams.add(name, value.toString())
        this
    }

    RestClient queryParam(String name, List<String> value) {
        if (value) queryParams.addAll(name, value)
        this
    }

    RestClient pathParam(String name, Object value) {
        if (value) pathParams.put(name, value)
        this
    }

    RestClient formDataParam(String name, Object value) {
        if (value) formDataParams.add(name, value)
        this
    }

    public <T> ResponseEntity<T> exchange(Class<T> responseType) {
        restTemplate.exchange(makeRequest(), responseType)
    }

    private RequestEntity<?> makeRequest() {
        URI uri = UriComponentsBuilder.fromUriString(path)
                .queryParams(queryParams)
                .build(pathParams);
        Object requestBody = formDataParams.isEmpty() ? this.body : formDataParams;
        return new RequestEntity<>(requestBody, headerParams, method, uri);
    }

}
