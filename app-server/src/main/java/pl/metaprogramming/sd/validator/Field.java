/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.validator;

import lombok.Getter;

import java.util.function.Function;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public class Field<T, R> {
    @Getter private final String name;
    private final Function<T, R> valueGetter;

    public Field(String name, Function<T, R> valueGetter) {
        this.name = name;
        this.valueGetter = valueGetter;
    }

    public R getValue(T context) {
        return context != null ? valueGetter.apply(context) : null;
    }
}
