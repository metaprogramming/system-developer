/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public class ValidationResult implements Serializable {

    @Getter private boolean stopped;
    @Getter private transient Object request;
    @Getter private Integer status;
    @Getter private List<ValidationError> errors = new ArrayList<>();

    public ValidationResult(Object request) {
        this.request = request;
    }

    public boolean isValid() {
        return status == null;
    }

    public boolean isFieldValid(String field) {
        for (ValidationError e : errors) {
            if (field != null && field.equals(e.getField())) return false;
        }
        return true;
    }

    public ValidationResult setStatus(int status) {
        this.status = status;
        return this;
    }

    public ValidationResult addError(ValidationError error) {
        status = error.getStatus() == null ? 400 : error.getStatus();
        errors.add(error);
        stopped = error.isStopValidation();
        return this;
    }

    public String getMessage() {
        return errors.get(0).getMessage();
    }

    public ValidationResult setError(int status, String message) {
        addError(ValidationError.builder().status(status).message(message).stopValidation(true).build());
        return this;
    }
}
