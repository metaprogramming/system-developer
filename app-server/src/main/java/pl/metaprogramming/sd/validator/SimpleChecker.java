/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.validator;

import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class SimpleChecker<I> implements Checker<I> {

    private final Function<ValidationContext<I>, Boolean> checker;
    private final Consumer<ValidationContext<I>> resultWriter;
    private final boolean checkNull;

    public SimpleChecker(
            Function<ValidationContext<I>, Boolean> checker,
            Consumer<ValidationContext<I>> resultWriter) {
        this.checker = checker;
        this.resultWriter = resultWriter;
        this.checkNull = false;
    }

    public SimpleChecker(
            Function<ValidationContext<I>, Boolean> checker,
            Consumer<ValidationContext<I>> resultWriter,
            boolean checkNull
            ) {
        this.checker = checker;
        this.resultWriter = resultWriter;
        this.checkNull = checkNull;
    }

    @Override
    public void check(ValidationContext<I> ctx) {
        if (!checker.apply(ctx)) {
            resultWriter.accept(ctx);
        }
    }

    @Override
    public boolean checkNull() {
        return checkNull;
    }
}