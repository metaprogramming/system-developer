/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.validator;

import java.util.stream.Collectors;
import javax.annotation.processing.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Generated("pl.metaprogramming.codegen")
public class ValidationException extends RuntimeException {

    private final ValidationResult result;

    @Override
    public String getMessage() {
        String errors = result.getErrors().stream()
               .map(err -> err.getField() == null ? err.getCode() : String.format("%s - %s", err.getField(), err.getCode()))
               .collect(Collectors.joining(", "));
        return String.format("Failed validation (%d) with errors: %s", result.getStatus(), errors);
    }
}
