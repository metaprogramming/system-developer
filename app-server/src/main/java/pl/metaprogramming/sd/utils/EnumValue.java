/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.utils;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface EnumValue {

    String getValue();

    default boolean equalsValue(String value) {
        return getValue().equals(value);
    }

    static <T extends EnumValue> T fromValue(String value, Class<T> enumClass) {
        if (value == null) return null;
        for (T e : enumClass.getEnumConstants()) {
            if (e.equalsValue(value)) return e;
        }
        throw new IllegalArgumentException(String.format("Unknown value '%s' of enum '%s'", value, enumClass.getCanonicalName()));
    }
}
