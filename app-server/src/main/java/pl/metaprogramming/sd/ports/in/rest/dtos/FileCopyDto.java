/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.ports.in.rest.dtos;

import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pl.metaprogramming.sd.utils.SerializationUtils;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class FileCopyDto {

    
    /**
     * Source file/directory path
     */
    @Nonnull private String src;
    
    /**
     * Destination file/directory path (should not exists earlier)
     */
    @Nonnull private String dst;
    
    /**
     * A flag indicating whether the source file/directory should be moved (not copied)
     */
    @Nonnull private Boolean isMove = SerializationUtils.toBoolean("false");
}
