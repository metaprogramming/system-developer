/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.ports.out;

import pl.metaprogramming.sd.ports.in.rest.dtos.DirectoryDto;
import pl.metaprogramming.sd.ports.in.rest.dtos.RestApiModelDto;

import java.io.File;
import java.util.List;

public interface WorkspaceRepository {

    DirectoryDto getFilesList();
    FileMetadata getMetadata(String path);
    void createDirectory(String path);
    void createFile(String path, byte[] content);
    File getFile(String path);
    byte[] getZippedDir(String path);
    void updateDir(String path, byte[] content);
    void delete(String path);
    void copy(String srcPath, String dstPath, boolean isMove);

    List<RestApiModelDto> getApiModels();
}
