/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest.validators;

import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.metaprogramming.sd.ports.in.rest.dtos.CopyFilesRequest;
import pl.metaprogramming.sd.ports.in.rest.dtos.FileCopyDto;
import pl.metaprogramming.sd.validator.Field;
import pl.metaprogramming.sd.validator.OperationId;
import pl.metaprogramming.sd.validator.ValidationContext;
import pl.metaprogramming.sd.validator.Validator;
import static pl.metaprogramming.sd.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class CopyFilesValidator extends Validator<CopyFilesRequest> {

    public static final Field<CopyFilesRequest,FileCopyDto> FIELD_BODY = new Field<>("body", CopyFilesRequest::getBody);

    private final FileCopyValidator fileCopyValidator;

    public void check(ValidationContext<CopyFilesRequest> ctx) {
        ctx.setBean(OperationId.COPY_FILES);
        ctx.checkRoot(FIELD_BODY, required(), fileCopyValidator);
    }
}
