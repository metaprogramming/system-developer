/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest.controllers;

import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.metaprogramming.sd.adapters.in.rest.validators.CopyFilesValidator;
import pl.metaprogramming.sd.ports.in.rest.ICopyFilesCommand;
import pl.metaprogramming.sd.ports.in.rest.dtos.CopyFilesRequest;
import pl.metaprogramming.sd.ports.in.rest.dtos.FileCopyDto;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class CopyFilesController {

    private final CopyFilesValidator copyFilesValidator;
    private final ICopyFilesCommand copyFilesCommand;

    /**
     * Copy, move or rename file or directory
     */
    @PostMapping(value = "/files-copy", consumes = {"application/json"})
    public ResponseEntity<Void> copyFiles(@RequestBody FileCopyDto body) {
        CopyFilesRequest request = new CopyFilesRequest()
                .setBody(body);
        copyFilesValidator.validate(request);
        copyFilesCommand.execute(request);
        return ResponseEntity.noContent().build();
    }
}
