/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.out;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.io.*;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static pl.metaprogramming.sd.adapters.out.FsUtils.mkdir;

@UtilityClass
public class ZipUtils {

    @SneakyThrows
    public static byte[] zipDir(File dir) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(os);
        zipDir(dir, "", zipOut);
        zipOut.close();
        return os.toByteArray();
    }

    @SneakyThrows
    public static File unzip(byte[] data, File outDir) {
        ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(data));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = new File(outDir, zipEntry.getName());
            if (zipEntry.isDirectory()) {
                mkdir(newFile);
            } else {
                mkdir(newFile.getParentFile());
                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    zis.transferTo(fos);
                }
            }
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
        return outDir;
    }

    @SneakyThrows
    private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) {
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            zipDir(fileToZip, fileName + "/", zipOut);
            return;
        }
        try (FileInputStream fis = new FileInputStream(fileToZip)) {
            ZipEntry zipEntry = new ZipEntry(fileName);
            zipOut.putNextEntry(zipEntry);
            fis.transferTo(zipOut);
        }
    }

    private static void zipDir(File dir, String pathInZip, ZipOutputStream zipOut) {
        File[] children = Objects.requireNonNull(dir.listFiles());
        for (File childFile : children) {
            zipFile(childFile, pathInZip + childFile.getName(), zipOut);
        }
    }

}
