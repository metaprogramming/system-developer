/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest.validators;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.metaprogramming.sd.ports.in.rest.dtos.FileCopyDto;
import pl.metaprogramming.sd.validator.Checker;
import pl.metaprogramming.sd.validator.Field;
import pl.metaprogramming.sd.validator.ValidationContext;
import static pl.metaprogramming.sd.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class FileCopyValidator implements Checker<FileCopyDto> {

    public static final Field<FileCopyDto,String> FIELD_SRC = new Field<>("src", FileCopyDto::getSrc);
    public static final Field<FileCopyDto,String> FIELD_DST = new Field<>("dst", FileCopyDto::getDst);
    public static final Field<FileCopyDto,Boolean> FIELD_IS_MOVE = new Field<>("isMove", FileCopyDto::getIsMove);

    public void check(ValidationContext<FileCopyDto> ctx) {
        ctx.check(FIELD_SRC, required());
        ctx.check(FIELD_DST, required());
        ctx.check(FIELD_IS_MOVE, required());
    }
}
