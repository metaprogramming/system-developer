/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest.controllers;

import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.metaprogramming.sd.adapters.in.rest.validators.DownloadFileValidator;
import pl.metaprogramming.sd.ports.in.rest.IDownloadFileQuery;
import pl.metaprogramming.sd.ports.in.rest.dtos.DownloadFileRequest;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadFileController {

    private final DownloadFileValidator downloadFileValidator;
    private final IDownloadFileQuery downloadFileQuery;

    /**
     * Zwraca treść pliku lub katalogu
     */
    @GetMapping(value = "/files", produces = {"text/plain", "application/zip"})
    public ResponseEntity<Object> downloadFile(@RequestParam String path) {
        DownloadFileRequest request = new DownloadFileRequest()
                .setPath(path);
        downloadFileValidator.validate(request);
        return downloadFileQuery.execute(request);
    }
}
