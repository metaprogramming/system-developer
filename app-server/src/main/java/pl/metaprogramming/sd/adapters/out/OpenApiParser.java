/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.out;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.metaprogramming.sd.ports.in.rest.dtos.*;

import java.nio.file.Path;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@SuppressWarnings({"java:S1168", "unchecked"})
@RequiredArgsConstructor
class OpenApiParser {

    private final Path filePath;
    private final String apiPath;

    RestApiModelDto parse() {
        try {
            Map<String, Object> map = new JsonYamlParser(filePath.toFile()).parse();
            if ("2.0".equals(map.get("swagger"))) return new Oas2ModelBuilder(map).make();
            if (map.containsKey("openapi")) return new Oas3ModelBuilder(map).make();
        } catch (Exception e) {
            log.error("Unable do process file {}", filePath, e);
        }
        return null;
    }

    @RequiredArgsConstructor
    private abstract class ApiModelBuilder {
        private static final String REF = "$ref";
        protected static final String DESCRIPTION = "description";
        private static final String REQUIRED = "required";
        protected static final String SCHEMA = "schema";
        protected static final String COMPONENTS = "components";
        protected static final String PARAMETERS = "parameters";
        protected static final String PROPERTIES = "properties";
        protected static final String HEADERS = "headers";
        protected static final String RESPONSES = "responses";

        protected final Map<String, Object> map;
        private final String kind;
        private final List<String> paramsPath;
        private final List<String> schemasPath;

        protected abstract DataTypeModelDto getParameterType(Map<String, Object> def);

        RestApiModelDto make() {
            return new RestApiModelDto()
                    .setPath(apiPath)
                    .setName(filePath.toFile().getName())
                    .setDescription(getApiDescription())
                    .setKind(kind)
                    .setOperations(collectOperations())
                    .setParameters(collectParameters())
                    .setSchemas(collectSchemas())
                    ;
        }

        private String getApiDescription() {
            var info = (Map<String, Object>) map.get("info");
            return Stream.of(info.get("title"), info.get(DESCRIPTION))
                    .filter(Objects::nonNull)
                    .map(Object::toString)
                    .collect(Collectors.joining("\n"));
        }

        private List<RestOperationModelDto> collectOperations() {
            var elements = (Map<String, Map<String, Object>>) map.get("paths");
            if (elements == null) return Collections.emptyList();
            var result = new ArrayList<RestOperationModelDto>();
            elements.forEach((path, pathDef) ->
                    pathDef.forEach((httpMethod, def) -> {
                                if (def instanceof Map) {
                                    var operationDef = (Map<String, Object>) def;
                                    result.add(new RestOperationModelDto()
                                            .setPath(fullPath("paths", path, httpMethod))
                                            .setHttpPath(path)
                                            .setHttpMethod(httpMethod.toUpperCase())
                                            .setSummary(asString(operationDef, "summary"))
                                            .setDescription(asString(operationDef, DESCRIPTION))
                                            .setParameters(collectOperationParams(operationDef))
                                            .setRequestBody(collectRequestPayloads(operationDef))
                                            .setResponseBody(collectResponsePayloads(operationDef))
                                    );
                                }
                            }
                    )
            );
            result.sort(Comparator.comparing(RestOperationModelDto::getHttpPath)
                    .thenComparing(RestOperationModelDto::getHttpMethod));
            return result;
        }

        private List<HttpParameterModelDto> collectParameters() {
            var params = (Map<String, Map<String, Object>>) getValue(map, paramsPath);
            if (params == null) return Collections.emptyList();
            List<HttpParameterModelDto> result = new ArrayList<>();
            params.forEach((paramDefName, def) ->
                    result.add(new HttpParameterModelDto()
                            .setPath(fullPath(paramsPath, paramDefName))
                            .setName(asString(def, "name"))
                            .setDescription(asString(def, DESCRIPTION))
                            .setIsRequired(isRequired(def))
                            .setKind(asString(def, "in").toUpperCase())
                            .setType(getParameterType(def)))
            );
            result.sort(Comparator.comparing(HttpParameterModelDto::getName));
            return result;
        }

        private List<DataSchemaModelDto> collectSchemas() {
            var schemas = (Map<String, Map<String, Object>>) getValue(map, schemasPath);
            if (schemas == null) return Collections.emptyList();
            List<DataSchemaModelDto> result = new ArrayList<>();
            schemas.forEach((schemaDefName, def) ->
                    result.add(new DataSchemaModelDto()
                            .setPath(fullPath(schemasPath, schemaDefName))
                            .setCode(schemaDefName)
                            .setTitle(asString(def, "title"))
                            .setDescription(asString(def, DESCRIPTION))
                            .setType(toDataTypeModel(def))
                    ));
            result.sort(Comparator.comparing(DataSchemaModelDto::getCode));
            return result;
        }

        private List<HttpParameterModelDto> collectOperationParams(Map<String, Object> operationDef) {
            return asList(operationDef, PARAMETERS).stream()
                    .map(o -> (Map<String, Object>) o)
                    .filter(def -> !"body".equals(asString(def, "in")))
                    .map(def -> def.containsKey(REF)
                            ? new HttpParameterModelDto().setPath(makeRef(asString(def, REF)))
                            : new HttpParameterModelDto()
                            .setName(asString(def, "name"))
                            .setDescription(asString(def, DESCRIPTION))
                            .setIsRequired(isRequired(def))
                            .setKind(asString(def, "in").toUpperCase())
                            .setType(getParameterType(def))
                    ).toList();
        }

        protected abstract List<HttpPayloadModelDto> collectRequestPayloads(Map<String, Object> operationDef);

        protected abstract List<HttpResponseModelDto> collectResponsePayloads(Map<String, Object> operationDef);

        private Object getValue(Map<String, Object> def, List<String> path) {
            Object v = def.get(path.get(0));
            if (v != null && path.size() > 1) {
                return getValue((Map<String, Object>) v, path.subList(1, path.size()));
            }
            return v;
        }

        protected List<Object> asList(Map<String, Object> def, String name) {
            Object value = def.get(name);
            return value != null ? (List<Object>) value : Collections.emptyList();
        }

        protected Map<String, Object> asMap(Map<String, Object> def, String name) {
            Object value = def.get(name);
            return value != null ? (Map<String, Object>) value : Collections.emptyMap();
        }

        protected String asString(Map<String, Object> def, String name) {
            return (String) def.get(name);
        }

        private Boolean isRequired(Map<String, Object> def) {
            return Boolean.TRUE.equals(def.get(REQUIRED)) ? true : null;
        }

        protected DataTypeModelDto toDataTypeModel(Map<String, Object> def) {
            if (def == null) return null;
            String dataType = getSimpleDataType(def);
            if ("array".equals(dataType)) {
                DataTypeModelDto itemsDataType = toDataTypeModel(asMap(def, "items"));
                return new DataTypeModelDto()
                        .setType("array:" + itemsDataType.getType())
                        .setProperties(itemsDataType.getProperties());
            }
            return new DataTypeModelDto().setType(dataType).setProperties(getDataTypeProperties(def));
        }

        private List<DataPropertyModelDto> getDataTypeProperties(Map<String, Object> def) {
            Object properties = def.get(PROPERTIES);
            if (properties == null) return null;
            List<Object> requiredProperties = asList(def, REQUIRED);
            return map((Map<String, Map<String, Object>>) properties, (property, propDef) ->
                    new DataPropertyModelDto()
                            .setName(property)
                            .setDescription(asString(propDef, DESCRIPTION))
                            .setIsRequired(requiredProperties.contains(property) ? true : null)
                            .setType(toDataTypeModel(propDef))
            );
        }

        private String getSimpleDataType(Map<String, Object> def) {
            if (def.containsKey("enum")) return "enum";
            String ref = asString(def, REF);
            if (ref != null) return "$ref:" + makeRef(ref);
            String format = asString(def, "format");
            if (format != null) return format;
            String type = asString(def, "type");
            return type == null ? "object" : type;
        }

        private String makeRef(String ref) {
            int idx = ref.indexOf('#');
            String refFilePath = idx > 0 ? getParentApiPath() : apiPath;
            return "%s%s".formatted(refFilePath, ref);
        }

        private String fullPath(List<String> baseParts, String part) {
            var partsList = new ArrayList<>(baseParts);
            partsList.add(part);
            return fullPath(partsList.toArray(new String[0]));
        }

        private String fullPath(String... parts) {
            StringBuilder buf = new StringBuilder();
            buf.append(apiPath).append('#');
            for (String part : parts) {
                buf.append('/');
                if (part.indexOf('/') >= 0) {
                    buf.append('`').append(part).append('`');
                } else {
                    buf.append(part);
                }
            }
            return buf.toString();
        }

        private String getParentApiPath() {
            int idx = apiPath.lastIndexOf('/');
            return idx < 0 ? "" : apiPath.substring(0, idx + 1);
        }

        protected <T, K, V> List<T> map(Map<K, V> map, BiFunction<K, V, T> mapper) {
            if (map == null) return List.of();
            return map.keySet().stream().map(k -> mapper.apply(k, map.get(k))).toList();
        }
    }

    private class Oas2ModelBuilder extends ApiModelBuilder {
        Oas2ModelBuilder(Map<String, Object> map) {
            super(map, "OAS2", List.of(PARAMETERS), List.of("definitions"));
        }

        private static final String CONSUMES = "consumes";
        private static final String PRODUCES = "produces";

        @Override
        protected DataTypeModelDto getParameterType(Map<String, Object> def) {
            return toDataTypeModel(def);
        }

        @Override
        protected List<HttpPayloadModelDto> collectRequestPayloads(Map<String, Object> operationDef) {
            Map<String, Object> schema = asList(operationDef, PARAMETERS).stream()
                    .map(p -> (Map<String, Object>) p)
                    .filter(p -> "body".equals(p.get("in")))
                    .findFirst()
                    .map(p -> asMap(p, SCHEMA))
                    .orElse(null);
            if (schema == null) return null;
            var consumes = (List<String>) operationDef.getOrDefault(CONSUMES, map.get(CONSUMES));
            return consumes == null ? null : consumes.stream()
                    .map(contentType -> new HttpPayloadModelDto()
                            .setContentType(contentType)
                            .setType(toDataTypeModel(schema))
                    ).toList();
        }

        @Override
        protected List<HttpResponseModelDto> collectResponsePayloads(Map<String, Object> operationDef) {
            Object responses = operationDef.get(RESPONSES);
            if (responses == null) return null;
            var produces = (List<String>) operationDef.getOrDefault(PRODUCES, map.get(PRODUCES));
            return map((Map<Object, Map<String, Object>>) responses, (status, def) ->
                    new HttpResponseModelDto()
                            .setStatus(status.toString())
                            .setDescription(asString(def, DESCRIPTION))
                            .setHeaders(map((Map<String, Map<String, Object>>) def.get(HEADERS), (headerName, headerDef) ->
                                    new DataPropertyModelDto()
                                            .setName(headerName)
                                            .setType(toDataTypeModel(headerDef))
                                            .setDescription(asString(headerDef, DESCRIPTION))
                            ))
                            .setContents(!def.containsKey(SCHEMA) || produces == null ? null : produces.stream()
                                    .map(contentType -> new HttpPayloadModelDto()
                                            .setContentType(contentType)
                                            .setType(toDataTypeModel(asMap(def, SCHEMA)))
                                    ).toList())
            );
        }
    }

    private class Oas3ModelBuilder extends ApiModelBuilder {
        Oas3ModelBuilder(Map<String, Object> map) {
            super(map, "OAS3", List.of(COMPONENTS, PARAMETERS), List.of(COMPONENTS, "schemas"));
        }

        private static final String CONTENT = "content";

        @Override
        protected DataTypeModelDto getParameterType(Map<String, Object> def) {
            return toDataTypeModel(asMap(def, SCHEMA));
        }

        @Override
        protected List<HttpPayloadModelDto> collectRequestPayloads(Map<String, Object> operationDef) {
            var requestBody = (Map<String, Object>) operationDef.get("requestBody");
            if (requestBody == null) return null;
            return map((Map<String, Map<String, Object>>) requestBody.get(CONTENT), (contentType, contentDef) ->
                    new HttpPayloadModelDto()
                            .setContentType(contentType)
                            .setType(toDataTypeModel(asMap(contentDef, SCHEMA)))
            );
        }

        @Override
        protected List<HttpResponseModelDto> collectResponsePayloads(Map<String, Object> operationDef) {
            Object responses = operationDef.get(RESPONSES);
            if (responses == null) return null;
            return map((Map<Object, Map<String, Object>>) responses, (status, def) ->
                    new HttpResponseModelDto()
                            .setStatus(status.toString())
                            .setDescription(asString(def, DESCRIPTION))
                            .setHeaders(map((Map<String, Map<String, Object>>) def.get(HEADERS), (headerName, headerDef) ->
                                    new DataPropertyModelDto()
                                            .setName(headerName)
                                            .setType(toDataTypeModel(headerDef))
                                            .setDescription(asString(headerDef, DESCRIPTION))
                            ))
                            .setContents(map((Map<String, Map<String, Object>>) def.get(CONTENT), (contentType, contentDef) ->
                                    new HttpPayloadModelDto()
                                            .setContentType(contentType)
                                            .setType(toDataTypeModel((Map<String, Object>) contentDef.get(SCHEMA)))
                            ))
            );
        }
    }
}
