/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.out;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;
import org.yaml.snakeyaml.resolver.Resolver;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Stream;

@RequiredArgsConstructor
class JsonYamlParser {

    private final File file;

    Map<String, Object> parse() {
        if (!file.isFile()) return Collections.emptyMap();
        if (isFileExt("json")) {
            return new JSONObject(readFile()).toMap();
        }
        if (isFileExt("yaml", "yml")) {
            var ops = new DumperOptions();
            return new Yaml(new Constructor(new LoaderOptions()), new Representer(ops), ops, new YamlTypeResolver())
                    .load(readFile());

        }
        return Collections.emptyMap();
    }

    private boolean isFileExt(String ... extensions) {
        return Stream.of(extensions).anyMatch(e -> file.getName().endsWith(e));
    }

    @SneakyThrows
    private String readFile() {
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }

    private static class YamlTypeResolver extends Resolver {
        @Override
        protected void addImplicitResolvers() {
            addImplicitResolver(Tag.BOOL, BOOL, "yYnNtTfFoO");
            addImplicitResolver(Tag.INT, INT, "-+0123456789");
            addImplicitResolver(Tag.NULL, NULL, "~nN0");
            addImplicitResolver(Tag.NULL, EMPTY, null);
        }
    }
}
