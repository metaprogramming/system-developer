/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest.controllers;

import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.metaprogramming.sd.adapters.in.rest.validators.GetFilesListValidator;
import pl.metaprogramming.sd.ports.in.rest.IGetFilesListQuery;
import pl.metaprogramming.sd.ports.in.rest.dtos.DirectoryDto;
import pl.metaprogramming.sd.ports.in.rest.dtos.GetFilesListRequest;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class GetFilesListController {

    private final GetFilesListValidator getFilesListValidator;
    private final IGetFilesListQuery getFilesListQuery;

    /**
     * Zwraca listę wszystkich plików opisujących system
     */
    @GetMapping(value = "/files-list", produces = {"application/json"})
    public ResponseEntity<DirectoryDto> getFilesList() {
        GetFilesListRequest request = new GetFilesListRequest();
        getFilesListValidator.validate(request);
        return ResponseEntity.ok(getFilesListQuery.execute(request));
    }
}
