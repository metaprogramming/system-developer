/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.out;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.metaprogramming.sd.ports.in.rest.dtos.DirectoryDto;
import pl.metaprogramming.sd.ports.in.rest.dtos.RestApiModelDto;
import pl.metaprogramming.sd.ports.out.FileMetadata;
import pl.metaprogramming.sd.ports.out.WorkspaceRepository;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Slf4j
@Component
public class WorkspaceRepositoryAdapter implements WorkspaceRepository {

    private final File root;

    WorkspaceRepositoryAdapter(@Value("${workspaceDirectory:out}") String workspaceDirectory) {
        root = new File(workspaceDirectory);
    }

    @Override
    public DirectoryDto getFilesList() {
        FsUtils.mkdir(root);
        return FsUtils.listDirectory(new DirectoryDto().setName("/"), root);
    }

    @Override
    public FileMetadata getMetadata(String path) {
        File file = getFile(path);
        return new FileMetadata()
                .setExists(file.exists())
                .setDirectory(file.isDirectory())
                .setName(file.getName())
                ;
    }

    @Override
    public void createDirectory(String path) {
        log.info("Create new dir: {}", path);
        FsUtils.mkdir(getFile(path));
    }

    @Override
    @SneakyThrows
    public void createFile(String path, byte[] content) {
        File file = getFile(path);
        FsUtils.mkdir(file.getParentFile());
        log.info("Create/update file: {}", path);
        Files.write(file.toPath(), content == null ? new byte[0] : content);
    }

    @Override
    @SneakyThrows
    public byte[] getZippedDir(String path) {
        return ZipUtils.zipDir(getFile(path));
    }

    @Override
    @SneakyThrows
    public void updateDir(String path, byte[] content) {
        log.info("update {} with zip content", path);
        ZipUtils.unzip(content, getFile(path));
    }

    @Override
    @SneakyThrows
    public void delete(String path) {
        FsUtils.delete(getFile(path));
    }

    @Override
    public File getFile(String path) {
        return new File(root, path);
    }

    @Override
    public void copy(String srcPath, String dstPath, boolean isMove) {
        log.info("{} {} to {}", (isMove ? "Move" : "Copy"), srcPath, dstPath);
        File src = new File(root, srcPath);
        File dst = new File(root, dstPath);
        if (isMove) {
            FsUtils.move(src, dst);
        } else {
            FsUtils.copy(src, dst);
        }
    }

    @Override
    @SneakyThrows
    public List<RestApiModelDto> getApiModels() {
        Path apisPath = new File(root, "apis").toPath();
        int basePathIdx = apisPath.toString().length() + 1;
        try (Stream<Path> fileStream = Files.walk(apisPath)) {
            return fileStream
                    .filter(file -> !Files.isDirectory(file))
                    .map(path -> new OpenApiParser(path, path.toString().substring(basePathIdx).replace("\\", "/")).parse())
                    .filter(Objects::nonNull)
                    .toList();
        }
    }
}
