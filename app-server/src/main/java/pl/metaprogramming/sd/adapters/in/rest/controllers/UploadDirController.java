/*
 * Copyright (c) 2025 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.in.rest.controllers;

import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.metaprogramming.sd.adapters.in.rest.validators.UploadDirValidator;
import pl.metaprogramming.sd.ports.in.rest.IUploadDirCommand;
import pl.metaprogramming.sd.ports.in.rest.dtos.UploadDirRequest;
import pl.metaprogramming.sd.utils.SerializationUtils;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadDirController {

    private final UploadDirValidator uploadDirValidator;
    private final IUploadDirCommand uploadDirCommand;

    /**
     * Aktualizuj zawartość katalogu
     */
    @PatchMapping(value = "/files", consumes = {"application/zip"})
    public ResponseEntity<Void> uploadDir(@RequestParam String path, @RequestBody Resource body) {
        UploadDirRequest request = new UploadDirRequest()
                .setPath(path)
                .setBody(SerializationUtils.toBytes(body));
        uploadDirValidator.validate(request);
        uploadDirCommand.execute(request);
        return ResponseEntity.noContent().build();
    }
}
