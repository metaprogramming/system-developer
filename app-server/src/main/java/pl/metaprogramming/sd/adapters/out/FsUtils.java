/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.adapters.out;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;
import pl.metaprogramming.sd.ports.in.rest.dtos.DirectoryDto;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

@UtilityClass
public class FsUtils {

    @SneakyThrows
    public static DirectoryDto listDirectory(DirectoryDto result, File dir) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir.toPath())) {
            stream.forEach(path -> {
                File f = path.toFile();
                if (f.isDirectory()) {
                    if (result.getDirs() == null) {
                        result.setDirs(new ArrayList<>());
                    }
                    result.getDirs().add(listDirectory(new DirectoryDto().setName(f.getName()), f));
                } else {
                    if (result.getFiles() == null) {
                        result.setFiles(new ArrayList<>());
                    }
                    result.getFiles().add(f.getName());
                }
            });
        }
        return result;
    }

    public static void mkdir(File dir) {
        if (!dir.isDirectory() && !dir.mkdirs()) {
            throw new IllegalStateException("Can't create directory: " + dir.getAbsolutePath());
        }
    }

    @SneakyThrows
    public static void delete(File file) {
        if (file.isDirectory()) {
            FileUtils.deleteDirectory(file);
        } else if (file.exists()) {
            FileUtils.delete(file);
        }
    }

    @SneakyThrows
    public static void move(File file, File dst) {
        Files.move(file.toPath(), dst.toPath());
    }

    @SneakyThrows
    public static void copy(File file, File dst) {
        if (file.isDirectory()) {
            FileUtils.copyDirectory(file, dst);
        } else {
            Files.copy(file.toPath(), dst.toPath());
        }
    }
}
