/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.process;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.metaprogramming.sd.ports.in.rest.IGetFilesListQuery;
import pl.metaprogramming.sd.ports.in.rest.dtos.DirectoryDto;
import pl.metaprogramming.sd.ports.in.rest.dtos.GetFilesListRequest;
import pl.metaprogramming.sd.ports.out.WorkspaceRepository;

import javax.annotation.Nonnull;
import java.util.*;

@Component
@RequiredArgsConstructor
public class GetFilesListQuery implements IGetFilesListQuery {

    private final WorkspaceRepository repository;

    @Override
    public DirectoryDto execute(@Nonnull GetFilesListRequest request) {
        DirectoryDto result = repository.getFilesList();
        ensure(result, "apis/private");
        ensure(result, "apis/public");
        ensure(result, "apis/external");
        ensure(result, "apps");
        sort(result);
        return result;
    }

    private void ensure(DirectoryDto root, String path) {
        int idx = path.indexOf('/');
        String name = idx > 0 ? path.substring(0, idx) : path;
        if (root.getDirs() == null) {
            root.setDirs(new ArrayList<>());
        }
        DirectoryDto dir = root.getDirs().stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseGet(() -> {
                    DirectoryDto newDir = new DirectoryDto().setName(name);
                    root.getDirs().add(newDir);
                    return newDir;
                });
        if (idx > 0) {
            ensure(dir, path.substring(idx + 1));
        }
    }

    private void sort(DirectoryDto dir) {
        Optional.ofNullable(dir.getFiles()).ifPresent(l -> l.sort(null));
        if (dir.getDirs() != null) {
            dir.getDirs().sort(Comparator.comparing(DirectoryDto::getName));
            dir.getDirs().forEach(this::sort);
        }
    }

}
