/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.process;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pl.metaprogramming.sd.ports.in.rest.IDownloadFileQuery;
import pl.metaprogramming.sd.ports.in.rest.dtos.DownloadFileRequest;
import pl.metaprogramming.sd.ports.out.FileMetadata;
import pl.metaprogramming.sd.ports.out.WorkspaceRepository;

import javax.annotation.Nonnull;

@Component
@RequiredArgsConstructor
public class DownloadFileQuery implements IDownloadFileQuery {

    private final WorkspaceRepository repository;

    @Override
    @SneakyThrows
    public ResponseEntity<Object> execute(@Nonnull DownloadFileRequest request) {
        FileMetadata file = repository.getMetadata(request.getPath());
        if (!file.isExists()) {
            return ResponseEntity.notFound().build();
        }
        if (file.isDirectory()) {
            byte[] zippedDir = repository.getZippedDir(request.getPath());
            return ResponseEntity.ok()
                    .header("Content-Disposition", String.format("attachment;fileName=%s.zip", file.getName()))
                    .contentType(MediaType.valueOf("application/zip")).body(zippedDir);
        }
        return ResponseEntity.ok()
                .header("Content-Disposition", String.format("attachment;fileName=%s", file.getName()))
                .contentType(MediaType.valueOf("text/plain;charset=UTF-8"))
                .body(new FileSystemResource(repository.getFile(request.getPath())));
    }

}
