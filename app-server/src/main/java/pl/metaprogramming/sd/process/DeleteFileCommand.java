/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.sd.process;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.metaprogramming.sd.ports.in.rest.IDeleteFileCommand;
import pl.metaprogramming.sd.ports.in.rest.dtos.DeleteFileRequest;
import pl.metaprogramming.sd.ports.out.WorkspaceRepository;

import javax.annotation.Nonnull;

@Component
@RequiredArgsConstructor
public class DeleteFileCommand implements IDeleteFileCommand {

    private final WorkspaceRepository repository;

    @Override
    public void execute(@Nonnull DeleteFileRequest request) {
        repository.delete(request.getPath());
    }

}
