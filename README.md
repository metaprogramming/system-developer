## Run code generator

Codegen is not working with java 17 (https://issues.apache.org/jira/browse/GROOVY-10137).
It is recommended to use java 11:
```
set JAVA_HOME=%USERPROFILE%\.jdks\corretto-11.0.15
```

To generate code execute command:
```
gradlew generator:run
```
