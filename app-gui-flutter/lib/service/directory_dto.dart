class DirectoryDto {
  DirectoryDto({
    required this.name,
    required this.files,
    required this.dirs,
  });

  final String name;
  final List<String>? files;
  final List<DirectoryDto>? dirs;

  factory DirectoryDto.fromJson(Map<String, dynamic> json) => DirectoryDto(
        name: json['name'],
        files: json['files'] == null
            ? null
            : (json['files'] as List<dynamic>)
                .map((e) => e.toString())
                .toList(),
        dirs: json['dirs'] == null
            ? null
            : (json['dirs'] as List<dynamic>)
                .map((e) => DirectoryDto.fromJson(e as Map<String, dynamic>))
                .toList(),
      );
}
