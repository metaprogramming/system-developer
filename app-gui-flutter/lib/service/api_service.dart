// ignore_for_file: avoid_web_libraries_in_flutter

import 'dart:convert';
import 'dart:html' as html;

import 'package:http/http.dart' as http;
import 'package:system_developer/service/directory_dto.dart';
import 'package:system_developer/widgets/event_log_controller.dart';

class ApiService {
  const ApiService(this.log);

  final EventLogController log;

  static const Duration _timeout = Duration(seconds: 1);
  static const String _baseUrl = 'http://localhost:8080';
  Uri _uri(String enpoint) => Uri.parse('$_baseUrl/$enpoint');
  Uri _fileUri(String path) => _uri('files?path=$path');

  Future<DirectoryDto> getFileList() {
    return _call(_Http.get, _uri('files-list'),
        (res) => DirectoryDto.fromJson(json.decode(res.body)));
  }

  Future<http.Response> getFileText(String path) {
    return _call(_Http.get, _fileUri(path), (res) => res);
  }

  Future<void> addFile(String path) {
    return saveFile(path, '');
  }

  Future<void> addDirectory(String path) {
    return saveFile(path, '');
  }

  Future<void> saveFile(String path, String body) {
    return _call(_Http.put, _fileUri(path), (res) {},
        body: body, encoding: utf8);
  }

  Future<void> delete(String path) {
    return _call(_Http.delete, _fileUri(path), (res) => res);
  }

  void download(String path) {
    var url = _fileUri(path).toString();
    html.AnchorElement anchorElement = html.AnchorElement(href: url);
    anchorElement.download = url;
    anchorElement.click();
  }

  Future<R> _call<R>(
    _Http method,
    Uri url,
    R Function(http.Response) mapper, {
    bool expectHttpSuccess = true,
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) {
    log.info('${method.name.toUpperCase()} $url');
    return _http(method, url, headers, body, encoding)
        .timeout(_timeout)
        .then((res) {
      var result = mapper(res);
      if (!expectHttpSuccess || res.statusCode < 200 || res.statusCode >= 300) {
        throw _unexpectedStatus(res);
      }
      log.info('SUCCESS $url (status: ${res.statusCode})');
      return result;
    }).catchError((e) {
      log.error('FAILED $url: $e');
    });
  }

  Future<http.Response> _http(
    _Http method,
    Uri url,
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  ) {
    switch (method) {
      case _Http.get:
        return http.get(url, headers: headers);
      case _Http.delete:
        return http.delete(url, headers: headers);
      case _Http.post:
        return http.post(url, headers: headers, body: body, encoding: encoding);
      case _Http.put:
        return http.put(url, headers: headers, body: body, encoding: encoding);
      default:
        throw Exception('Should never happend');
    }
  }

  Exception _unexpectedStatus(http.Response res) =>
      Exception("Unexpected HTTP status ${res.statusCode}: ${res.body}");
}

enum _Http {
  get,
  delete,
  post,
  put;
}
