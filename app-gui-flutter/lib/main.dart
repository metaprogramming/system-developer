import 'package:flutter/material.dart';
import 'package:system_developer/widgets/browser.dart';

void main() {
  runApp(const SystemDeveloperApp());
}

class SystemDeveloperApp extends StatelessWidget {
  const SystemDeveloperApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'System Developer',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('System Developer'),
      ),
      body: const SizedBox(
        height: double.infinity,
        child: Browser(),
      ),
    );
  }
}
