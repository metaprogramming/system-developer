import 'package:flutter/material.dart';
import 'package:system_developer/widgets/file_editor_controller.dart';

class DirView extends StatelessWidget {
  const DirView(
    this.controller, {
    super.key,
  });

  final FileEditorController controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: SelectableText(controller.path),
            ),
            Expanded(
              child: ButtonBar(
                children: [
                  ElevatedButton(
                    child: const Text('Pobierz'),
                    onPressed: () {
                      controller.download();
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Usuń'),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (ctx) => AlertDialog(
                          title:
                              const Text("Czy na pewno chcesz usunąć katalog"),
                          content: const Text(
                              "Usunięcie katalogu jest nie odwracalne"),
                          actions: [
                            TextButton(
                              child: const Text('Usuń'),
                              onPressed: () {
                                controller.delete();
                                Navigator.pop(context);
                              },
                            ),
                            ElevatedButton(
                              child: const Text('Anuluj'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Dodaj plik'),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => _AddFileDialog(
                            (name) => controller.addFile(name)),
                      );
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Dodaj katalog'),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => _AddFileDialog(
                            (name) => controller.addDir(name),
                            isDir: true),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class _AddFileDialog extends StatelessWidget {
  const _AddFileDialog(this.addHandler, {this.isDir = false});

  final Function(String) addHandler;
  final bool isDir;

  @override
  Widget build(BuildContext context) {
    var textController = TextEditingController();
    return AlertDialog(
      scrollable: true,
      title: Text('Dodaj ${isDir ? 'katalog' : 'plik'}'),
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          child: Column(
            children: [
              TextFormField(
                controller: textController,
                autofocus: true,
                decoration: InputDecoration(
                  labelText: 'Nazwa ${isDir ? 'katalogu' : 'pliku'}',
                ),
                onFieldSubmitted: (value) {
                  addHandler(value);
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Anuluj'),
        ),
        ElevatedButton(
          child: const Text("Dodaj"),
          onPressed: () {
            addHandler(textController.text);
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}
