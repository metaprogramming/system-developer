import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:system_developer/widgets/dir_view.dart';
import 'package:system_developer/widgets/file_editor_controller.dart';

import 'file_view.dart';

class FileEditor extends StatefulWidget {
  const FileEditor(this.controller, {super.key});

  final FileEditorController controller;

  @override
  State<StatefulWidget> createState() => _FileEditor();
}

class _FileEditor extends State<FileEditor> {
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      log('_FileEditor.listener: ${widget.controller.path}');
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    log('_FileEditor.build: ${widget.controller.path}');
    if (!widget.controller.isReady) {
      return Column(children: const []);
    } else if (widget.controller.isDir) {
      return DirView(widget.controller);
    } else {
      return FileView(widget.controller);
    }
  }
}
