import 'dart:developer';

import 'package:flutter/cupertino.dart';

class EventLogController extends ChangeNotifier {
  final List<Log> events = [];
  static const int _limit = 20;

  void error(String message) {
    _add(message, true);
  }

  void info(String message, {bool error = false}) {
    _add(message, false);
  }

  void _add(String message, bool error) {
    log("$message [${error ? 'ERROR' : 'INFO'}]");
    final stamp = DateTime.now().toIso8601String().substring(11);
    events.insert(0, Log('[$stamp] $message', error));
    if (events.length > _limit) events.removeLast();
    notifyListeners();
  }
}

class Log {
  Log(this.message, this.error);
  final String message;
  final bool error;
}
