import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_simple_treeview/flutter_simple_treeview.dart';
import 'package:http/http.dart' as http;
import 'package:system_developer/service/api_service.dart';
import 'package:system_developer/service/directory_dto.dart';
import 'package:system_developer/widgets/event_log_controller.dart';

class FileTreeController extends ChangeNotifier {
  FileTreeController(EventLogController logger) {
    _apiService = ApiService(logger);
  }

  final TreeController treeController = TreeController(allNodesExpanded: false);
  late final ApiService _apiService;
  DirectoryDto? _files;

  String path = '/';
  bool isDir = true;
  bool loading = false;
  bool failed = false;

  bool isReady() {
    return !loading && !failed;
  }

  DirectoryDto get files {
    return _files!;
  }

  void navigete(String path) {
    log('FileTreeController.naviagete $path');
    this.path = path;
    isDir = path.endsWith('/');
    for (int i = 1; i < path.length - 2; i++) {
      if (path[i] == '/') {
        treeController.expandNode(Key(path.substring(0, i + 1)));
      }
    }
    treeController.expandNode(Key(path));
    notifyListeners();
  }

  void load({String? navigateToPath}) {
    loading = true;
    failed = false;
    log('FileTreeController.load $path');
    _apiService.getFileList().then((value) {
      _files = value;
      loading = false;
      if (navigateToPath != null) {
        navigete(navigateToPath);
      } else {
        notifyListeners();
      }
    }).catchError((e) {
      loading = false;
      failed = true;
      notifyListeners();
    });
  }

  void addFile(String name) {
    _add(name);
  }

  void setBody(String body) {
    _apiService.saveFile(path, body);
  }

  Future<http.Response> getBody() {
    return _apiService.getFileText(path);
  }

  void addDir(String name) {
    _add("$name/");
  }

  void delete() {
    if (path == '/') return;
    _apiService.delete(path);
    path = path.substring(0, path.lastIndexOf('/'));
    isDir = true;
    load();
  }

  void _add(String name) {
    isDir = name.endsWith('/');
    path = "$path/$name/";
    _apiService.saveFile(path, '');
    load();
  }

  void download() {
    _apiService.download(path);
  }
}
