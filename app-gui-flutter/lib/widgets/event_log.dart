import 'package:flutter/material.dart';
import 'package:system_developer/widgets/event_log_controller.dart';

class EventLog extends StatefulWidget {
  const EventLog(this.controller, {super.key});

  final EventLogController controller;

  @override
  State<StatefulWidget> createState() => _EventLogState();
}

class _EventLogState extends State<EventLog> {
  final int _buildLap = 20;
  int _lastBuildTime = DateTime.now().millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      int moment = DateTime.now().millisecondsSinceEpoch - _lastBuildTime;
      Future.delayed(
          Duration(milliseconds: moment < _buildLap ? _buildLap - moment : 0),
          () {
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _lastBuildTime = DateTime.now().millisecondsSinceEpoch;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widget.controller.events
            .map(
              (e) => SelectableText(
                e.message,
                style: e.error ? const TextStyle(color: Colors.red) : null,
              ),
            )
            .toList(),
      ),
    );
  }
}
