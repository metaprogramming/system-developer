import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_simple_treeview/flutter_simple_treeview.dart';
import 'package:system_developer/service/directory_dto.dart';
import 'package:system_developer/widgets/file_tree_controller.dart';

class FileTree extends StatefulWidget {
  const FileTree(this.controller, {super.key});

  final FileTreeController controller;

  @override
  State createState() => _FileTreeState();
}

class _FileTreeState extends State<FileTree> {
  void select(String path) {
    log('_FileTreeState.select $path');
    widget.controller.navigete(path);
  }

  DirectoryDto get _root {
    return widget.controller.files;
  }

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    log('_FileTreeState.build: ${widget.controller.isReady()}, ${widget.controller.path}');
    if (widget.controller.loading) return const Text('loading');
    if (widget.controller.failed) return const Text('failed');
    return SingleChildScrollView(
      child: TextButtonTheme(
        data: TextButtonThemeData(
          style: TextButton.styleFrom(
            alignment: Alignment.centerLeft,
          ),
        ),
        child: TreeView(
          iconSize: 10,
          indent: 10,
          nodes: _makeChildrenNodes(_root.name, _root),
          treeController: widget.controller.treeController,
        ),
      ),
    );
  }

  List<TreeNode> _makeChildrenNodes(String path, DirectoryDto dir) {
    return [
      if (dir.dirs != null) ...dir.dirs!.map((e) => _makeNode(path, e)),
      if (dir.files != null) ...dir.files!.map((e) => _makeLeaf(path, e)),
    ];
  }

  TreeNode _makeNode(String parentPath, DirectoryDto dir) {
    String path = '$parentPath${dir.name}/';
    return TreeNode(
      content: _NodeText(this, path, dir.name),
      key: Key(path),
      children: _makeChildrenNodes(path, dir),
    );
  }

  TreeNode _makeLeaf(String parentPath, String file) {
    String path = '$parentPath$file';
    return TreeNode(
      content: _NodeText(this, path, file),
      key: Key(path),
    );
  }
}

class _NodeText extends StatelessWidget {
  const _NodeText(this.tree, this.path, this.text);

  final String path;
  final String text;
  final _FileTreeState tree;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => tree.select(path),
      child: Text(
        text,
        style: tree.widget.controller.path.startsWith(path)
            ? const TextStyle(
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.bold,
              )
            : null,
      ),
    );
  }
}
