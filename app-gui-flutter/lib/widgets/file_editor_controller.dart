import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:system_developer/service/api_service.dart';
import 'package:system_developer/widgets/event_log_controller.dart';
import 'package:system_developer/widgets/file_tree_controller.dart';

class FileEditorController extends ChangeNotifier {
  FileEditorController(
    this.fileTreeController,
    EventLogController logger,
  ) {
    _apiService = ApiService(logger);
    fileTreeController.addListener(() {
      log("FileEditorController.fileTreeController.listener ${fileTreeController.path} - dir: ${fileTreeController.isDir}");
      path = fileTreeController.path;
      isDir = fileTreeController.isDir;
      notifyListeners();
    });
  }

  late final ApiService _apiService;
  final FileTreeController fileTreeController;
  String path = '/';
  bool isDir = true;
  bool isReady = true;

  void addFile(String name) {
    _add(name);
  }

  void setBody(String body) {
    _apiService.saveFile(path, body);
  }

  Future<http.Response> getBody() {
    return _apiService.getFileText(path);
  }

  void addDir(String name) {
    _add("$name/");
  }

  void delete() {
    if (path == '/') return;
    _apiService.delete(path).then((value) {
      path = path.substring(0, path.substring(0, path.length - 1).lastIndexOf('/') + 1);
      isDir = true;
      fileTreeController.load(navigateToPath: path);
    });
  }

  void _add(String name) {
    path = "$path$name";
    _apiService.saveFile(path, '').then((value) {
      fileTreeController.load(navigateToPath: path);
    });
  }

  void download() {
    _apiService.download(path);
  }
}
