import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:split_view/split_view.dart';
import 'package:system_developer/widgets/event_log.dart';
import 'package:system_developer/widgets/event_log_controller.dart';
import 'package:system_developer/widgets/file_editor.dart';
import 'package:system_developer/widgets/file_editor_controller.dart';
import 'package:system_developer/widgets/file_tree.dart';
import 'package:system_developer/widgets/file_tree_controller.dart';

class Browser extends StatefulWidget {
  const Browser({super.key});

  @override
  State createState() => _BrowserState();
}

class _BrowserState extends State<Browser> {
  final EventLogController logger = EventLogController();
  late final FileTreeController _treeController;

  @override
  void initState() {
    super.initState();
    _treeController = FileTreeController(logger);
    _treeController.load();
  }

  @override
  Widget build(BuildContext context) {
    log('_BrowserState.build');
    return SplitView(
      viewMode: SplitViewMode.Vertical,
      controller: SplitViewController(weights: [0.9]),
      children: [
        SplitView(
          viewMode: SplitViewMode.Horizontal,
          controller: SplitViewController(weights: [0.2]),
          children: [
            FileTree(_treeController),
            FileEditor(FileEditorController(_treeController, logger))
          ],
        ),
        EventLog(logger),
      ],
    );
  }
}
