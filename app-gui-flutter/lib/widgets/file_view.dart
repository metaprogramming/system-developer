import 'package:flutter/material.dart';
import 'package:system_developer/widgets/file_editor_controller.dart';

class FileView extends StatefulWidget {
  const FileView(this.controller, {super.key});

  final FileEditorController controller;

  @override
  State<StatefulWidget> createState() => _FileViewState();
}

class _FileViewState extends State<FileView> {
  final TextEditingController _controller = TextEditingController();
  String? _currentPath;
  bool _loading = true;
  bool _failed = false;
  bool _unsupported = false;

  @override
  Widget build(BuildContext context) {
    _load();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: SelectableText(widget.controller.path),
            ),
            Expanded(
              child: ButtonBar(
                children: [
                  ElevatedButton(
                    child: const Text('Zapisz'),
                    onPressed: () {
                      widget.controller.setBody(_controller.text);
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Usuń'),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (ctx) => AlertDialog(
                          title: const Text("Czy na pewno chcesz usunąć plik"),
                          content:
                              const Text("Usunięcie pliku jest nie odwracalne"),
                          actions: [
                            TextButton(
                              child: const Text('Usuń'),
                              onPressed: () {
                                widget.controller.delete();
                                Navigator.pop(context);
                              },
                            ),
                            ElevatedButton(
                              child: const Text('Anuluj'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Zablokuj'),
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          ],
        ),
        if (_loading)
          const Text('Loading')
        else if (_failed)
          const Text('Load error')
        else if (_unsupported)
          const Text('Unsupported file type')
        else
          Expanded(
            child: Container(
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.blueGrey)),
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.all(4.0),
              child: TextField(
                controller: _controller,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                minLines: null,
                maxLines: null,
                expands: true,
              ),
            ),
          ),
      ],
    );
  }

  void _load() async {
    if (_currentPath != widget.controller.path) {
      _currentPath = widget.controller.path;
      _loading = true;
      _failed = false;
      _unsupported = false;
      widget.controller.getBody().then((response) {
        if (response.headers['content-type']!.startsWith('text/plain')) {
          _controller.text = response.body;
        } else {
          _unsupported = true;
        }
        _loading = false;
        setState(() {});
      }).catchError((e) {
        _loading = false;
        _failed = true;
        setState(() {});
      });
    }
  }
}
