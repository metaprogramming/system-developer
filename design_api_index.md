
# Ścieżka (techniczna) do elementu

<API_file_path> -> <OPERATION_ID: POST /pets> -> <PARAM_LOCATION: QUERY, PATH, HEADER, COOKIE> param <PARAM_NAME>

- api.yaml#/paths/\`/pets\`/post/parameters[0]
- api.yaml#/paths/\`/pets\`/post/parameters[name = 'xyz']
- api.yaml#/paths/\`/pets\`/post/parameters[$ref = 'xyz']

Reguły:
 - ścieżka jest unikalna i wprost odnosi się do lokalizacji pliku i miejsca w tym pliku 
 - elementy ścieżki są rozdzielone znakiem '/'
 - ścieżka wewnątrz pliku jest oddzielona od ścieżki do pliku znakiem '#'
 - jeżeli element ścieżki zawiera znak '/', to ścieżka jest escepowana znakiem '`'
 - adresowanie elementów listy (w kwadratowym nawiasie)
   - indeksem
   - po wartości element klucz

# Nazwa elementu API

Powinna być precyzyjna i umiarkowanie treściwa, ale bez zbytecznych detali.
- operacja: `<http method> <endpoint path>` (zamiast operationId lub ścieżki w pliku "/paths/\`/pets\`/post")
- parametr operacji: `<location> param <name>` | `<name> <location> param`
  - Co prezentować, gdy jest tylko referencja do definicji parametru?
  - Czy prezentować od razu typ danych?
- request body: `request <content-type> <data type|ref path>`
  - bez sensu prezentować standardowy content-type, ale co znaczy standardowy?
  - zamiast `ref path` może prezentować nazwę elementu (struktury danych), do której jest referencja
- response body: `response <content-type> <data type|ref path>`
- response header: `reasponse HEADER <name>`
- struktura danych: `<data schema key>`
- pole w strukturze danych: `<property key>`

# Typy danych
Prezentacja typów danych:
- `<format>` jeśli w OAS jest obecny `format`, to wystarczy go prezentować.
- `enum[<lista możliwych wartości>]`
- `<type>`, gdy nie ma ustawionego ani `format` ani `enum`
- `<data schema key>`, gdy zdefiniowana struktura danych
- `array of <nazwa typu określona na podstawie powyższych zasad>`, gdy jest `array`

# Powiązania między elementami API
W specyfikacji OAS powiązanie jest realizowane przez `$ref`.
Specyfikacja OAS umożliwia inline'owe definiowanie elementów w ramach innych elementów:
- parametry operacji w definicji operacji
- enumeracje, a nawet struktury danych (`type: object`)


# Po co index API?
Czemu nie wystarczy przeglądanie plików tekstowych?
- do wyszukiwania elementów API w edytorze dokumentacji (aby wstawić go do dokumentacji)
  - wyszukiwanie może być zawężane przez edytor dokumentacji, dokumentacja może być zawężona do kilku specyfikacji API, a w danym kontekście nawet do dwóch struktur danych (mapowanie komunikatu wchodzącego do usługi na komunik wychodzący),
  - prezentacja tak wyszukanych elementów powinna być możliwie krótka i treściwa
- do wyszukiwania elementów API w widoku API
  - zawężenie wyszukiwania:
    - do elementów określonego typu
    - do elementów z określonego zestawu specyfikacji
  - prezentacja wyszukanego elementu z kontekstem użycia
- do konstrukcji widoku API
  ... przeglądanie API jako drzewa: katalogi, pliki, operacje, struktury danych
    - prezentacja użycia danego elementu API
        - w dokumentacji... dokumentacja będzie się odnosiła do elementu API w formacie linku markdown `[apis://<API_ELEMENT_PATH](custom name)`
        - w innych elementach API: użycie struktury danych w innych strukturach, czy w operacjach jako (część) komunikatu in/out
        - w aplikacjach... która aplikacja implementuje te API, a która go używa
    - prezentacja tekstowej reprezentacji elementu API, wycinek ze specyfikacji, a ewentualnie zakładka z edytorem całego pliku specyfikacji, gdzie ten element się znajduje
    - uproszczona prezentacja elementu modelu (operacji, struktury danych)... 
      - jak w swagger-ui,
      - diagram klas dla wizualizacji struktur danych

# Jak ma być prezentowany rezultat wyszukiwania?
To zależy od kontekstu.

Jeśli wyszukiwanie następuje z poziomu edytora dokumentacji na potrzeby podpowiedzi

# Wymagania co do danych w indeksie
- ścieżka do elementu (unikalny klucz)
    - ścieżka będzie umieszczona w dokumentacji, musi być też indeks użycia danej ścieżki
    - na podstawie ścieżki będzie prezentacja drzewa API (... chyba dotyczy to tylko katalogów w których są rozmieszczone specyfikacje API)
- krótki opis (name)
- rozbudowany opis (description)
- typ elementu używany do budowy widoku API, ścieżki nie można użyć bo mogą być różne specyfikacje (różnica jest nawet między OAS2 i OAS3)
- referencje (ścieżki) do elementów używanych w tym elemencje
  - będą to ścieżki, lub bezpośrednio element modelu (który też musi mieć ścieżkę) jeżeli jego definicja jest zagnieżdżona w opisie elementu nadrzędnego
- reference do elementów, które używają tego elementu
  np. dla struktury danych będzie to np operacja, która używa tej struktury jako request lub response, albo innej struktury, która zagnieżdża w sobie tą strukturę

## API
- title
- description
- referencje do innych elementów API

## Operacje
- `<http method> <http path>` prezentacja razem, ale sortowanie najpierw po path, a potem method
- summary, description i operationId
- parametry (referencje i inline'owe definicje: name + location + isRequired + data-type)
- req body (lista content-type + data-type)
- res body (lista content-type + data-type)
- res headers (name + data-type)

## Parametr
- path
- name
- location
- isRequired
- data-type

Parametr może być zdefiniowany poza operacją, jako byt niezależny, do którego jest referencja z operacji, to wówczas ma ustawione pole 'path'.
Jeżeli z operacji jest referencja do zewnętrznie zdefiniowanego parametru, to jest ustawiony tylko 'path'.

## data-type
- `$ref`
- scalar (na podstawie type + format + enum + pattern)
- inline object (lista property)
- array of data-type
```json
{
  "type": "$ref|object|string|array:<type>|...",
  "properties": [
    {
      "name": "prop_name",
      "description": "Property description",
      "isRequired": true,
      "type": "<data-type>"
    }
  ],
  "enum": ["val1", "val2"]
}
```

Czy dla enum'ów, dawać listę w strukturze modelu, czy zawierać ją w opisie?
Umieszczenie wartości enum'a w strukturze modelu umożliwi referowanie do tej wartości w dokumentacji.
Jednak używanie enum'ów same w sobie jest ryzykowne, bo dodanie nowej wartości psuje kompatybilność wsteczną.
Zamiast enum'ów można by używać słowników (konfiguracja biznesowa).
W związku, z czym raczej należy ograniczyć wsparcie dla enum'ów do niezbędnego minimum.

## data-type definition
- name (key in OAS3:/components/schemas | OAS2:/definitions)
- description
- data-type

## data-type object property
- name
- description
- isRequired
- data-type

# Widok API
- dir
  - api_file (type = API
    - lista operacji
      - prezentacja `<http method> <http path>`, ale sortowanie najpierw path, a potem method)
    - schemas
      - lista definicji danych (OAS2 /definitions; OAS3 /components/schemas)
    - parameters
      - lista definicji parametrów (OAS2 /parameters; OAS3 /components/parameters; nie są brane pod uwagę parametry zdefiniowane bezpośrednio w definicji operacji)

Prezentowane części API w widoku API nie muszą, a może nawet nie powinny zawierać zbyt dużo szczegółów.
Wszystkie szczegóły są w specyfikacjach tekstowych.
Oferowany widok powinien dawać lepsze rozeznanie w całości (z jakąś formą diagramu). 