import {
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  SimpleChanges,
  Component,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { SwaggerUIBundle, SwaggerUIStandalonePreset } from 'swagger-ui-dist';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { BrowserService } from '../browser.service';

@Component({
  selector: 'sd-swagger-ui',
  standalone: true,
  imports: [ScrollPanelModule],
  templateUrl: './swagger-ui.component.html',
  styleUrl: './swagger-ui.component.scss',
})
export class SwaggerUiComponent implements AfterViewInit, OnChanges {
  @ViewChild('swaggerui') swaggerDom!: ElementRef<HTMLDivElement>;
  @Input() path!: string;
  isLoaded: boolean = false;

  constructor(
    private readonly browserService: BrowserService,
    private readonly _elementRef: ElementRef
  ) {}

  @HostListener('scroll', ['$event'])
  onScroll(event: any) {
    if (this.isLoaded) {
      this.browserService.swaggerUiScroll.set(
        this.path,
        event.target.scrollTop
      );
    }
  }

  ngOnChanges(_: SimpleChanges): void {
    this._load();
  }

  ngAfterViewInit(): void {
    this._load();
  }

  private _load() {
    if (this.swaggerDom) {
      this.isLoaded = false;
      SwaggerUIBundle({
        url: this.browserService.getFileUrl(this.path),
        domNode: this.swaggerDom.nativeElement,
        presets: [SwaggerUIBundle['presets'].apis, SwaggerUIStandalonePreset],
        layout: 'BaseLayout',
        onComplete: () => {
          this._elementRef.nativeElement.scrollTop =
            this.browserService.swaggerUiScroll.get(this.path);
          this.isLoaded = true;
        },
      });
    }
  }
}
