import { ComponentType } from '@angular/cdk/portal';
import { Injectable, inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  matDialog = inject(MatDialog);

  constructor() {}

  openDialog<T>(data: any, component: ComponentType<T>): Observable<any> {
    return this.matDialog
      .open(component, {
        data: data,
        disableClose: true,
      })
      .afterClosed();
  }
}
