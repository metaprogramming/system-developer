import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, of } from 'rxjs';

export interface DirectoryDto {
  name: string;
  dirs: Array<DirectoryDto>;
  files: Array<string>;
}

export interface FileModel {
  path: string;
  content: string;
  language: string;
  newContent: string;
  updated: boolean;
  previewScroll: number;
  monacoModel: any;
  monacoViewState: any;
  exists: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class BrowserService {
  private readonly basePath = 'http://localhost:8080'
  private readonly openedFiles: Map<string, FileModel> = new Map();
  swaggerUiScroll: Map<string, number> = new Map();
  updateFileSubject: BehaviorSubject<string> = new BehaviorSubject('/');
  updatePathSubject: BehaviorSubject<string> = new BehaviorSubject('/');
  constructor(private readonly http: HttpClient) {}

  getFileUrl(path: string): string {
    return `${this.basePath}/files?path=${path}`;
  }

  getFile(path: string): Observable<FileModel> {
    if (this.openedFiles.has(path)) {
      return of(this.openedFiles.get(path)!);
    }
    return this.http.get(this.getFileUrl(path), { responseType: 'text' }).pipe(
      map((value) => {
        let result = {
          path: path,
          content: value,
          language: this.getFileLanguage(path),
          newContent: value,
          exists: true,
        } as FileModel;
        this.openedFiles.set(path, result);
        return result;
      })
    );
  }

  saveFile(path: string, body: any) {
    this.saveFileObservable(path, body).subscribe((res) => {});
  }

  saveFileObservable(path: string, body: any): Observable<any> {
    console.log('going to save file: ' + path);
    if (this.openedFiles.get(path)) {
      this.openedFiles.get(path)!.content = body;
    }
    return this.http.put(this.getFileUrl(path), body).pipe((res) => {
      console.log('file uploaded: ' + path);
      this.updateFileSubject.next(path);
      return res;
    });
  }

  patchDir(path: string, body: any): Observable<any> {
    return this.http
      .patch(this.getFileUrl(path), body, {
        headers: { 'Content-Type': 'application/zip' },
      })
      .pipe((res) => {
        console.log('dir patched: ' + path);
        let pathToInvalidate = new Array<string>();
        this.openedFiles.forEach((_, path) => {
          if (path.startsWith(path)) pathToInvalidate.push(path);
        });
        pathToInvalidate.forEach((path) => this.openedFiles.delete(path));
        this.updateFileSubject.next(path);
        return res;
      });
  }

  delete(path: string): Observable<any> {
    console.log('going to dlete file: ' + path);
    return this.http.delete(this.getFileUrl(path));
  }

  copy(srcPath: string, dstPath: string, isMove: boolean): Observable<any> {
    console.log('going to copy file: ' + srcPath);
    return this.http.post(`${this.basePath}/files-copy`, {
      src: srcPath,
      dst: dstPath,
      isMove: isMove
    });
  }

  getFileLanguage(path: string): string {
    let ext = path.substring(path.lastIndexOf('.') + 1);
    if (ext === 'md') return 'markdown';
    return ext;
  }

  getFileList(): Observable<DirectoryDto> {
    return this.http.get<DirectoryDto>(`${this.basePath}/files-list`);
  }
}
