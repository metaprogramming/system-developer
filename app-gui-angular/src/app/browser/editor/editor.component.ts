import {
  Component,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EditorComponent as MonacoEditorComponent } from 'ngx-monaco-editor-v2';
import { BrowserService, FileModel } from '../browser.service';
import { catchError, Observable, of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

declare var monaco: any;

@Component({
  selector: 'sd-editor',
  standalone: true,
  imports: [FormsModule, MonacoEditorComponent],
  templateUrl: './editor.component.html',
  styleUrl: './editor.component.scss',
})
export class EditorComponent implements OnChanges {
  @Input() path!: string;
  originCode: string = '';
  @Output() code?: string;
  @Output() uppdated = false;
  private _editor: any;
  private _file!: FileModel;

  constructor(private readonly browserService: BrowserService) {}

  monacoInitialized(editor: any) {
    console.log('monacoInitialized');
    this._editor = editor;
    editor.onDidChangeModelContent((change: any) => {
      this.uppdated = true;
      //if auto save should works in 'ngOnDestroy' the value should be stored earlier because editor will be destroyed
      //this._file.newContent = this._editor.getValue();
    });
    this.browserService.updateFileSubject.subscribe((updatedPath) => {
      if (updatedPath === this.path && this._editor?.getModel() && this._file) {
        this._editor.getModel().setValue(this._file.content);
      }
    });
  }

  ngOnChanges(_: SimpleChanges): void {
    this.saveState().subscribe();
    this._load();
  }

  private _load() {
    this.browserService
      .getFile(this.path)
      .pipe(
        catchError((_: HttpErrorResponse) => {
          return of({
            path: this.path,
            content: '',
            language: this.browserService.getFileLanguage(this.path),
          } as FileModel);
        })
      )
      .subscribe((file) => this._showCode(file));
  }

  private _showCode(file: FileModel) {
    if (typeof this._editor !== 'undefined') {
      this._file = file;
      if (typeof file.monacoModel === 'undefined') {
        file.monacoModel = monaco.editor.createModel(
          file.content,
          file.language
        );
      }
      this._editor.setModel(file.monacoModel);
      if (typeof file.monacoViewState !== 'undefined') {
        this._editor.restoreViewState(file.monacoViewState);
      }
      this.uppdated = false;
    } else {
      setTimeout(() => this._showCode(file), 100);
    }
  }

  testRevealPosition() {
    let index = this._editor.getModel().getValue().indexOf("/pet/findByTags")
    let pos = this._editor.getModel().getPositionAt(index);
    this._editor.revealPositionInCenter(pos);
  }

  saveState(): Observable<any> {
    if (
      typeof this._editor !== 'undefined' &&
      typeof this._file !== 'undefined'
    ) {
      this._file.monacoViewState = this._editor.saveViewState();
      this._file.newContent = this._editor.getValue();
      if (this._file.content !== this._file.newContent) {
        this._file.content = this._file.newContent;
        this._file.updated = true;
        return this.browserService.saveFileObservable(
          this._file.path,
          this._file.content
        );
      }
    }
    return of('no update');
  }

  editorOptions = {
    scrollBeyondLastLine: false,
    lineHeight: 20,
    fontSize: 14,
    wordWrap: 'on',
    wrappingIndent: 'indent',
    theme: 'vs-dark',
    automaticLayout: true,
  };
}
