//https://github.com/angular/material.angular.io/blob/main/src/app/shared/example-viewer/example-viewer.ts
import {
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { EditorComponent } from '../editor/editor.component';
import { SwaggerUiComponent } from '../swagger-ui/swagger-ui.component';
import { FileNode } from '../file-tree/file-tree.component';
import { BrowserService } from '../browser.service';
import { MarkdownModule } from 'ngx-markdown';
import { catchError, map, Observable, of } from 'rxjs';
import { CommonModule } from '@angular/common';

export type Modes = 'preview' | 'editor';
export type Types = 'dir' | 'oas' | 'markdown' | 'txt' | 'unknown';

@Component({
  selector: 'sd-file-view',
  standalone: true,
  imports: [
    CommonModule,
    EditorComponent,
    MarkdownModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    SwaggerUiComponent,
  ],
  templateUrl: './file-view.component.html',
  styleUrl: './file-view.component.scss',
})
export class FileViewComponent implements OnChanges {
  mode: Modes = 'preview';
  type: Types = 'dir';
  @Input() node!: FileNode;
  @ViewChild(EditorComponent) editor!: EditorComponent;
  modeByType: Map<Types, Modes> = new Map([['txt', 'editor']]);
  isLoaded: boolean = false;
  isReadmeExists: boolean = false;

  constructor(private readonly browserService: BrowserService) {}

  ngOnChanges(_: SimpleChanges) {
    let name = this.node.name;
    if (this.node.path.endsWith('/')) {
      this.type = 'dir';
      this.isLoaded = true;
      this.isReadmeExists = false;
      this._checkReadmeExists().subscribe((exists) => {
        this.isReadmeExists = exists;
        this.isLoaded = true;
      });
    } else if (name.endsWith('.md')) {
      this.type = 'markdown';
    } else if (name.endsWith('.txt')) {
      this.type = 'txt';
    } else if (
      name.endsWith('.json') ||
      name.endsWith('.yaml') ||
      name.endsWith('.yml')
    ) {
      this.type = 'oas';
    } else {
      this.type = 'unknown';
    }
    this.mode = this.modeByType.get(this.type) ?? 'preview';
  }

  toggleMode() {
    if (this.editor) {
      this.editor.saveState().subscribe(() => this._toggleMode());
    } else {
      this._toggleMode();
    }
  }

  _toggleMode() {
    this.mode = this.mode === 'preview' ? 'editor' : 'preview';
    this.modeByType.set(this.type, this.mode);
  }

  isSwitchableMode(): boolean {
    return ['oas', 'dir', 'markdown'].indexOf(this.type) != -1;
  }

  isUpdated(): boolean {
    return this.editor?.uppdated;
  }

  save() {
    this.editor.saveState().subscribe((_) => (this.editor.uppdated = false));
    if (this.type === 'dir' && !this.isReadmeExists) {
      this.browserService.updatePathSubject.next(this.node.path);
    }
  }

  getUrl(): string {
    return this.browserService.getFileUrl(this.node.path);
  }

  onUpload($event: any) {
    const file = $event.target.files[0];
    (file.arrayBuffer() as Promise<any>).then((contnet) => {
      if (this.node.path.endsWith('/')) {
        if (file.name.endsWith('.zip')) {
          this.browserService
            .patchDir(this.node.path, contnet)
            .subscribe((_) => {
              this.browserService.updatePathSubject.next(this.node.path);
            });
        } else {
          this.browserService
            .saveFileObservable(this.node.path + file.name, contnet)
            .subscribe((_) => {
              if (!this.node.hasChild(file.name)) {
                this.browserService.updatePathSubject.next(this.node.path);
              }
            });
        }
      } else {
        this.browserService.saveFile(this.node.path, contnet);
      }
      $event.target.value = '';
    });
  }

  // for dir type

  getReamdePath(): string {
    return this.node.path + 'readme.md';
  }

  getReadmeUrl(): string {
    return this.browserService.getFileUrl(this.getReamdePath());
  }

  bookmark() {
    throw new Error('Method not implemented.');
  }

  private _checkReadmeExists(): Observable<boolean> {
    if (this.node.hasChild('readme.md')) return of(true);
    return this.browserService.getFile(this.node.path + 'readme.md').pipe(
      map((fileModel) => fileModel.exists),
      catchError((err) => of(false))
    );
  }
}
