import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTree, MatTreeModule } from '@angular/material/tree';
import { BrowserService, DirectoryDto } from '../browser.service';
import { MatTooltip } from '@angular/material/tooltip';
import { DialogService } from '../dialog/dialog.service';
import { ConfirmDialogComponent } from '../dialog/confirm-dialog.component';
import { InputDialogComponent } from '../dialog/input-dialog.component';

export class FileNode {
  name!: string;
  path!: string;
  children?: FileNode[];

  constructor(name: string, path: string, children?: FileNode[]) {
    this.name = name;
    this.path = path;
    this.children = children;
  }

  hasChild(name: string): boolean {
    return this.children?.find((file) => file.name === name) != null;
  }

  allowCopyInto(node: FileNode): boolean {
    return this !== node && !this.hasChild(node.name);
  }
}

@Component({
  selector: 'sd-file-tree',
  standalone: true,
  imports: [
    MatTreeModule,
    MatButtonModule,
    MatIconModule,
    MatTooltip,
  ],
  templateUrl: './file-tree.component.html',
  styleUrl: './file-tree.component.scss',
})
export class FileTreeComponent implements OnInit {
  @Output() change: EventEmitter<FileNode> = new EventEmitter<FileNode>();
  dataSource = new Array<FileNode>();
  selected?: FileNode;
  focused?: FileNode;
  toCopy?: FileNode;
  toMove?: FileNode;
  @ViewChild(MatTree) tree!: MatTree<FileNode, string>;

  constructor(
    private readonly browserService: BrowserService,
    private readonly dialogService: DialogService
  ) {
    this.browserService.updatePathSubject.subscribe((_) => {
      this.refresh(this.selected);
    });
  }

  ngOnInit() {
    this.browserService.getFileList().subscribe((root) => {
      this.dataSource = this._toNode(root, '/');
      this.dataSource.splice(0, 0, new FileNode('/', '/', this.dataSource));
      if (this.selected === undefined) {
        this.selected = this.dataSource[0];
        this.onSelect(this.selected);
      }
    });
  }

  onSelect(node: FileNode) {
    if (!this.tree.isExpanded(node)) {
      this.tree.toggle(node);
    }
    this.selected = node;
    this.change.emit(node);
  }

  onMouseEnter(node: FileNode) {
    this.focused = node;
  }

  onMouseLeave() {
    this.focused = undefined;
  }

  refresh(node?: FileNode) {
    console.log('tree refresh: ' + node?.path);
    this.ngOnInit();
  }

  onDelete(node: FileNode) {
    this.dialogService
      .openDialog(
        {
          title: 'Confirmation',
          message: `Are you sure you want to delete ${node.path}?`,
        },
        ConfirmDialogComponent
      )
      .subscribe((confirmation) => {
        if (confirmation) {
          this.browserService.delete(node.path).subscribe((_) => {
            this.ngOnInit();
          });
        }
      });
  }

  onCutStart(node: FileNode) {
    this.toCopy = undefined;
    this.toMove = node;
  }

  onCopyStart(node: FileNode) {
    this.toCopy = node;
    this.toMove = undefined;
  }

  onPaste(node: FileNode) {
    if (this.toCopy) {
      this._copyDialog(this.toCopy, node, false);
    } else if (this.toMove) {
      this._copyDialog(this.toMove, node, true);
    }
  }

  onRename(node: FileNode) {
    this.dialogService
      .openDialog(
        {
          title: 'Rename',
          inputValue: node.name,
          confirmLable: 'Rename',
        },
        InputDialogComponent
      )
      .subscribe((result) => {
        if (result) {
          const suffix = node.path.endsWith('/') ? node.name + '/' : node.name;
          const newPath =
            node.path.substring(0, node.path.lastIndexOf(suffix)) + result;
          this.browserService.copy(node.path, newPath, true).subscribe((_) => {
            this.ngOnInit();
          });
        }
      });
  }

  private _toNode(dir: DirectoryDto, path: string): FileNode[] {
    const result = new Array<FileNode>();
    if (dir.dirs) {
      dir.dirs.forEach((d) => {
        let dirPath = path + d.name + '/';
        result.push(new FileNode(d.name, dirPath, this._toNode(d, dirPath)));
      });
    }
    if (dir.files) {
      dir.files.forEach((f) => result.push(new FileNode(f, path + f)));
    }
    return result;
  }

  private _copyDialog(node: FileNode, dstDir: FileNode, isMove: boolean) {
    this.dialogService
      .openDialog(
        {
          title: 'Confirmation',
          message: isMove
            ? `Are you sure you want to move ${node.path} into ${dstDir.path}?`
            : `Are you sure you want to copy ${node.path} into ${dstDir.path}?`,
        },
        ConfirmDialogComponent
      )
      .subscribe((confirmation) => {
        if (confirmation) {
          this.toCopy = undefined;
          this.toMove = undefined;
          this.browserService
            .copy(node.path, `${dstDir.path}${node.name}`, isMove)
            .subscribe((_) => {
              this.ngOnInit();
            });
        }
      });
  }

  childrenAccessor = (node: FileNode) =>
    node.path === '/' ? [] : node.children ?? [];
  expansionKey = (node: FileNode) => node.path;
  hasChild = (_: number, node: FileNode) => !!node.children;
  isRoot = (_: number, node: FileNode) => node.path === '/';
  indent = '15';
}
