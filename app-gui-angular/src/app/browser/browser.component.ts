import { Component, ViewChild } from '@angular/core';
import { AngularSplitModule } from 'angular-split';
import { FileViewComponent } from './file-view/file-view.component';
import {
  FileTreeComponent,
  FileNode,
} from './file-tree/file-tree.component';

export type ViewType = 'dir' | 'OAS' | 'other';

@Component({
  selector: 'sd-browser',
  standalone: true,
  imports: [
    AngularSplitModule,
    FileTreeComponent,
    FileViewComponent,
  ],
  templateUrl: './browser.component.html',
  styleUrl: './browser.component.scss',
})
export class BrowserComponent {
  @ViewChild(FileTreeComponent) tree!: FileTreeComponent;

  path: string = '/';
  node?: FileNode;
  view?: ViewType;

  onSelect(node: FileNode) {
    this.node = node;
    this.path = node.path;
    this.view = this.path.endsWith('/') ? 'dir' : 'other';
  }

  onDirUpdate(node: FileNode) {
    this.tree.refresh(node);
  }
}
