import {
  afterNextRender,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import mermaid from 'mermaid';
import { ModelNode, RestOperationModel } from '../api-browser.service';
import svgPanZoom from 'svg-pan-zoom';

@Component({
  selector: 'sd-operation-view',
  standalone: true,
  imports: [],
  templateUrl: './operation-view.component.html',
  styleUrl: './operation-view.component.scss',
})
export class OperationViewComponent implements OnChanges {
  @Input() node!: ModelNode;
  @ViewChild('mermaid') _mermaidDom!: ElementRef<HTMLDivElement>;
  private _mermaidInitialized = false;

  constructor() {
    afterNextRender({
      read: () => {
        console.log('afterNextRender: ' + this.node.path);
        mermaid.initialize({
          securityLevel: 'loose',
          theme: 'dark',
          startOnLoad: false,
        });
        this._mermaidInitialized = true;
        (window as any).callBackFn = (nodeId: any) => {
          console.log('Hit callBackFn', nodeId);
        };
        this.draw();
      },
    });
  }

  ngOnChanges(_: SimpleChanges): void {
    console.log(`ngOnChanges: ${this.node.path}, _mermaidInitialized: ${this._mermaidInitialized}`);
    if (this._mermaidInitialized) this.draw();
  }

  async draw() {
    const graphDefinition = this.operationGraph(
      this.node.value as RestOperationModel
    );
    const { svg, bindFunctions } = await mermaid.render(
      'graphDiv',
      graphDefinition
    );
    this._mermaidDom.nativeElement.innerHTML = svg;
    // This can also be written as `bindFunctions?.(element);` using the `?` shorthand.
    if (bindFunctions) {
      bindFunctions(this._mermaidDom.nativeElement);
    }
    svgPanZoom('#graphDiv');
  }

  operationGraph(model: RestOperationModel): string {
    const timestamp = new Date();
    const buf : Array<string> = [];
    buf.push('graph LR');
    buf.push(`b["${timestamp}"]`);
    new GraphElemBuilder('OP', `${model.httpMethod} ${model.httpPath}`)
      .addItem(model.summary)
      .addItem(model.description)
      .push(buf);

    if (model.parameters.length > 0) {
      const paramsElem = new GraphElemBuilder('PARAMS', 'parameters');
      model.parameters.forEach((p) => {
        if (p.path) paramsElem.addItem(p.path);
        else
          paramsElem.addField(
            `<span class="parameterType">(${p.kind})</span>&nbsp;${p.name}`,
            p.type.type,
            p.isRequired
          );
      });
      paramsElem.push(buf)
      buf.push('OP ---- PARAMS');
    }

    if (model.requestBody?.length > 0) {
      new GraphElemBuilder("REQ", "request").push(buf);
      model.requestBody.forEach((r) => {
        buf.push(`REQ -- "${r.contentType}" --> ${r.type.type}`);
      });
      buf.push('OP ---- REQ');
    }

    if (model.responseBody?.length > 0) {
      new GraphElemBuilder("RES", "response").push(buf);
      model.responseBody.forEach((r) => {
        buf.push(`RES ---- ${r.status}`);
        if (r.contents?.length > 0) {
          r.contents.forEach((c) => {
            const resConId = `${c.type.type}`;
            buf.push(`${r.status} -- "${c.contentType}" --> ${resConId}`);
          });
        }
        if (r.headers?.length > 0) {
          const headers = new GraphElemBuilder(`${r.status}HEADERS`, "headers");
          r.headers.forEach((h) => {
            headers.addField(h.name, h.type.type, h.isRequired)
          });
          headers.push(buf);
          buf.push(`${r.status} ---- ${headers.id}`);
        }
      });
      buf.push('OP ---- RES');
    }
    const result = buf.join('\n');
    console.log(result);
    return result;
  }
}

class GraphElemBuilder {
  private readonly items: Array<string> = [];

  constructor(readonly id: string, private readonly title: string) {
    this.id = id;
    this.title = title;
  }

  make(): string {
    let items = '';
    if (this.items.length > 0) {
      items = '<ul>';
      this.items.forEach((i) => (items += `<li>${i}</li>`));
      items += '</ul>';
    }
    return `${this.id}["<span class="header">${this.title}</span>${items}"]`;
  }
  push(buf: Array<string>) {
    buf.push(this.make());
  }
  addItem(item?: string): this {
    if (item) this.items.push(item);
    return this;
  }
  addField(name: string, type: string, isRequired: boolean): this {
    return this.addItem(
      `${name}${
        isRequired ? '<span class="requiredMarkt">*</span>' : ''
      }:&nbsp;${type}`
    );
  }
}
