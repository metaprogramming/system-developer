import { Component } from '@angular/core';
import { ApiTreeComponent, ModelNode } from './api-tree/api-tree.component';
import { AngularSplitModule } from 'angular-split';
import { FileViewComponent } from "../browser/file-view/file-view.component";
import { OperationViewComponent } from './operation-view/operation-view.component';

@Component({
  selector: 'sd-api-browser',
  standalone: true,
  imports: [ApiTreeComponent, AngularSplitModule, FileViewComponent, OperationViewComponent],
  templateUrl: './api-browser.component.html',
  styleUrl: './api-browser.component.scss'
})
export class ApiBrowserComponent {

  node?: ModelNode;

  onSelect(node: ModelNode) {
    this.node = node;
  }

}
