import { Component, Input } from '@angular/core';
import { ApiTreeComponent } from './api-tree/api-tree.component';
import { AngularSplitModule } from 'angular-split';
import { FileViewComponent } from '../browser/file-view/file-view.component';
import { OperationViewComponent } from './operation-view/operation-view.component';
import { ApiBrowserService, ModelNode } from './api-browser.service';

@Component({
  selector: 'sd-api-browser',
  standalone: true,
  imports: [
    ApiTreeComponent,
    AngularSplitModule,
    FileViewComponent,
    OperationViewComponent,
  ],
  templateUrl: './api-browser.component.html',
  styleUrl: './api-browser.component.scss',
})
export class ApiBrowserComponent {
  node?: ModelNode;

  constructor(private readonly apiBrowserSerivce: ApiBrowserService) {}

  @Input()
  set path(path: string) {
    this.apiBrowserSerivce.getApiNode(path).subscribe((v) => {
      this.node = v;
    });
  }
}
