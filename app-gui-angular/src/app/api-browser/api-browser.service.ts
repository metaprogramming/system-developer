import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, of } from 'rxjs';

export interface ResApiModel {
  path: string;
  name: string;
  kind?: string;
  description?: string;
  operations: Array<RestOperationModel>;
  schemas: Array<DataSchemaModel>;
  parameters: Array<HttpParameterModel>;
}

export interface RestOperationModel {
    path: string;
    httpPath: string;
    httpMethod: string;
    summary?: string;
    description?: string;
    parameters: Array<HttpParameterModel>;
    requestBody: Array<HttpPayloadModel>;
    responseBody: Array<HttpResponseModel>;
}

export interface HttpPayloadModel {
    contentType: string;
    type: DataTypeModel;
}

export interface HttpResponseModel {
    status: string;
    description?: string;
    contents: Array<HttpPayloadModel>;
    headers: Array<DataPropertyModel>;
}

export interface DataSchemaModel {
  path?: string;
  code?: string;
  title?: string;
  description?: string;
  type?: DataTypeModel;
}

export interface DataPropertyModel {
  name: string;
  description?: string;
  isRequired: boolean;
  type: DataTypeModel;
}

export interface DataTypeModel {
  type: string;
  properties?: Array<DataPropertyModel>;
}

export interface HttpParameterModel {
  path?: string;
  name?: string;
  kind?: string;
  description?: string;
  isRequired: boolean;
  type: DataTypeModel;
}

@Injectable({
  providedIn: 'root',
})
export class ApiBrowserService {
  private readonly basePath = 'http://localhost:8080';
  constructor(private readonly http: HttpClient) {}

  getAllApis(): Observable<Array<ResApiModel>> {
    return this.http.get<Array<ResApiModel>>(`${this.basePath}/apis/all`);
  }
}
