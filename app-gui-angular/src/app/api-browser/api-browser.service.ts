import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  firstValueFrom,
  from,
  map,
  Observable,
  of,
} from 'rxjs';
import { FileNode } from '../browser/file-tree/file-tree.component';
import { synchronized } from 'synchronized-ts';

export interface RestApiModel {
  path: string;
  name: string;
  kind?: string;
  description?: string;
  operations: Array<RestOperationModel>;
  schemas: Array<DataSchemaModel>;
  parameters: Array<HttpParameterModel>;
}

export interface RestOperationModel {
  path: string;
  httpPath: string;
  httpMethod: string;
  summary?: string;
  description?: string;
  parameters: Array<HttpParameterModel>;
  requestBody: Array<HttpPayloadModel>;
  responseBody: Array<HttpResponseModel>;
}

export interface HttpPayloadModel {
  contentType: string;
  type: DataTypeModel;
}

export interface HttpResponseModel {
  status: string;
  description?: string;
  contents: Array<HttpPayloadModel>;
  headers: Array<DataPropertyModel>;
}

export interface DataSchemaModel {
  path?: string;
  code?: string;
  title?: string;
  description?: string;
  type?: DataTypeModel;
}

export interface DataPropertyModel {
  name: string;
  description?: string;
  isRequired: boolean;
  type: DataTypeModel;
}

export interface DataTypeModel {
  type: string;
  properties?: Array<DataPropertyModel>;
}

export interface HttpParameterModel {
  path?: string;
  name?: string;
  kind?: string;
  description?: string;
  isRequired: boolean;
  type: DataTypeModel;
}

export type Kinds =
  | 'dir'
  | 'group'
  | 'api'
  | 'operation'
  | 'parameter'
  | 'schema';

export class ModelNode {
  name!: string;
  path!: string;
  children?: ModelNode[];
  value?: any;
  kind: Kinds;
  fileNode?: FileNode;
  parent?: ModelNode;

  constructor(
    kind: Kinds,
    name: string,
    path: string,
    value?: any,
    children?: ModelNode[],
    parent?: ModelNode
  ) {
    this.kind = kind;
    this.name = name;
    this.path = path;
    this.value = value;
    this.children = children;
    this.parent = parent;
  }

  static dirNode(
    name: string,
    path: string,
    children?: ModelNode[]
  ): ModelNode {
    return new ModelNode('dir', name, path, undefined, children || new Array());
  }

  static groupNode(name: string, path: string, children: ModelNode[]) {
    return new ModelNode('group', name, path, undefined, children);
  }

  static apiNode(api: RestApiModel, children: ModelNode[]) {
    return new ModelNode('api', api.name, api.path, api, children);
  }

  static leafNode(kind: Kinds, name: string, path: string, value: any) {
    return new ModelNode(kind, name, path, value);
  }

  hasChild(name: string): boolean {
    return this.children?.find((file) => file.name === name) != null;
  }

  isDirOrFile(): boolean {
    return ['dir', 'api'].includes(this.kind);
  }

  toFileNode(): FileNode {
    if (this.fileNode == undefined) {
      const path =
        this.kind == 'dir' && !this.path.endsWith('/')
          ? `${this.path}/`
          : this.path;
      this.fileNode = new FileNode(
        this.name,
        `/apis/${path !== '/' ? path : ''}`
      );
    }
    return this.fileNode;
  }
}

@Injectable({
  providedIn: 'root',
})
export class ApiBrowserService {
  private readonly basePath = 'http://localhost:8080';
  private cache?: Array<ModelNode> = undefined;
  private index: Map<string, ModelNode> = new Map();
  constructor(private readonly http: HttpClient) {}

  @synchronized
  async getAllApis(): Promise<Array<ModelNode>> {
    return firstValueFrom(this._loadAll());
  }

  getApiNode(path: string): Observable<ModelNode> {
    return from(this.getAllApis()).pipe(map((_) => this.index.get(path)!));
  }

  private _loadAll(): Observable<Array<ModelNode>> {
    if (this.cache != undefined) return of(this.cache);
    return this.http.get<Array<RestApiModel>>(`${this.basePath}/apis/all`).pipe(
      map((value) => {
        this.index = new Map();
        const result = this._toModelTree(value);
        const rootNode = ModelNode.dirNode('/', '/', []);
        result.splice(0, 0, rootNode);
        this.cache = result;
        this._collectIndex(this.cache);
        return this.cache;
      })
    );
  }

  private _toModelTree(apiList: Array<RestApiModel>): ModelNode[] {
    const result = new Array<ModelNode>();
    const nodeIdx: Map<string, ModelNode> = new Map();
    apiList.forEach((api) => {
      const children = new Array<ModelNode>();
      this._collectOperations(api, children);
      this._collectParameters(api, children);
      this._collectSchemas(api, children);
      const node = ModelNode.apiNode(api, children);
      const splittedPath = this._splitPath(api.path);
      if (splittedPath.length == 1) {
        result.push(node);
      } else {
        const parent = this._getNode(splittedPath[1], nodeIdx, result);
        parent.children!.push(node);
      }
    });
    return result;
  }

  private _getNode(
    path: string,
    nodeIdx: Map<string, ModelNode>,
    rootNodes: Array<ModelNode>
  ): ModelNode {
    if (nodeIdx.get(path)) return nodeIdx.get(path)!;
    const splittedPath = this._splitPath(path);
    const result = ModelNode.dirNode(splittedPath[0], path);
    if (splittedPath.length == 2) {
      this._getNode(splittedPath[1], nodeIdx, rootNodes).children!.push(result);
    } else {
      rootNodes.push(result);
    }
    nodeIdx.set(path, result);
    return result;
  }

  private _splitPath(path: string): Array<string> {
    const pathEndIdx = path.lastIndexOf('/');
    if (pathEndIdx < 0) return [path];
    return [path.substring(pathEndIdx + 1), path.substring(0, pathEndIdx)];
  }

  private _collectOperations(api: RestApiModel, result: Array<ModelNode>) {
    if (api.operations && api.operations.length > 0) {
      const children = new Array<ModelNode>();
      api.operations.forEach((o) => {
        const name = `${o.httpMethod}\u00A0${o.httpPath}`;
        children.push(new ModelNode('operation', name, o.path, o));
      });
      result.push(
        ModelNode.groupNode('operations', `${api.path}#/paths/`, children)
      );
    }
  }

  private _collectSchemas(api: RestApiModel, result: Array<ModelNode>) {
    if (api.schemas && api.schemas.length > 0) {
      const children = new Array<ModelNode>();
      api.schemas.forEach((s) =>
        children.push(new ModelNode('schema', s.code!, s.path!, s))
      );
      result.push(
        ModelNode.groupNode('models', `${api.path}!schemas`, children)
      );
    }
  }

  private _collectParameters(api: RestApiModel, result: Array<ModelNode>) {
    if (api.parameters && api.parameters.length > 0) {
      const children = new Array<ModelNode>();
      api.parameters.forEach((p) =>
        children.push(new ModelNode('parameter', p.name!, p.path!, p))
      );
      result.push(
        ModelNode.groupNode('parameters', `${api.path}!parameters`, children)
      );
    }
  }

  private _collectIndex(nodes: ModelNode[], parent?: ModelNode) {
    nodes.forEach((node) => {
      if (parent) node.parent = parent;
      this.index.set(node.path, node);
      if (node.children) this._collectIndex(node.children, node);
    });
  }
}
