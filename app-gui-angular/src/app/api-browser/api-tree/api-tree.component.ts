import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltip } from '@angular/material/tooltip';
import { MatTree, MatTreeModule } from '@angular/material/tree';
import {
  ApiBrowserService,
  ResApiModel as RestApiModel,
} from '../api-browser.service';
import { FileNode } from '../../browser/file-tree/file-tree.component';

export type Kinds =
  | 'dir'
  | 'group'
  | 'api'
  | 'operation'
  | 'parameter'
  | 'schema';

export class ModelNode {
  name!: string;
  path!: string;
  children?: ModelNode[];
  value?: any;
  kind: Kinds;
  fileNode?: FileNode;

  constructor(
    kind: Kinds,
    name: string,
    path: string,
    value?: any,
    children?: ModelNode[]
  ) {
    this.kind = kind;
    this.name = name;
    this.path = path;
    this.value = value;
    this.children = children;
  }

  static dirNode(
    name: string,
    path: string,
    children?: ModelNode[]
  ): ModelNode {
    return new ModelNode('dir', name, path, undefined, children || new Array());
  }

  static groupNode(name: string, path: string, children: ModelNode[]) {
    return new ModelNode('group', name, path, undefined, children);
  }

  static apiNode(api: RestApiModel, children: ModelNode[]) {
    return new ModelNode('api', api.name, api.path, api, children);
  }

  static leafNode(kind: Kinds, name: string, path: string, value: any) {
    return new ModelNode(kind, name, path, value);
  }

  hasChild(name: string): boolean {
    return this.children?.find((file) => file.name === name) != null;
  }

  isDirOrFile(): boolean {
    return ['dir', 'api'].includes(this.kind);
  }

  toFileNode(): FileNode {
    if (this.fileNode == undefined) {
      const path =
        this.kind == 'dir' && !this.path.endsWith('/')
          ? `${this.path}/`
          : this.path;
      this.fileNode = new FileNode(
        this.name,
        `/apis/${path !== '/' ? path : ''}`
      );
    }
    return this.fileNode;
  }
}

@Component({
  selector: 'sd-api-tree',
  standalone: true,
  imports: [MatTreeModule, MatButtonModule, MatIconModule, MatTooltip],
  templateUrl: './api-tree.component.html',
  styleUrl: './api-tree.component.scss',
})
export class ApiTreeComponent implements OnInit {
  @Output() change: EventEmitter<ModelNode> = new EventEmitter<ModelNode>();
  dataSource = new Array<ModelNode>();
  selected?: ModelNode;
  focused?: ModelNode;
  @ViewChild(MatTree) tree!: MatTree<ModelNode, string>;
  constructor(private readonly apiBrowserSerivce: ApiBrowserService) {}

  ngOnInit(): void {
    this.apiBrowserSerivce.getAllApis().subscribe((root) => {
      this.dataSource = this._toNode(root);
      this.dataSource.splice(
        0,
        0,
        ModelNode.dirNode('/', '/', this.dataSource)
      );
      if (this.selected === undefined) {
        this.selected = this.dataSource[0];
        this.selected = this.dataSource[1].children![0].children![0].children![0].children![0];
        this.selected = this.dataSource[1].children![0].children![0].children![0].children![4];
        this.selected = this.dataSource[2].children![2].children![0].children![1];
        this.onSelect(this.selected);
      }
    });
  }

  onSelect(node: ModelNode) {
    if (!this.tree.isExpanded(node)) {
      this.tree.toggle(node);
    }
    this.selected = node;
    this.change.emit(node);
  }

  onMouseEnter(node: ModelNode) {
    this.focused = node;
  }

  onMouseLeave() {
    this.focused = undefined;
  }

  refresh(node?: ModelNode) {
    console.log('tree refresh: ' + node?.path);
    this.ngOnInit();
  }

  private _toNode(apiList: Array<RestApiModel>): ModelNode[] {
    const result = new Array<ModelNode>();
    const nodeIdx: Map<string, ModelNode> = new Map();
    apiList.forEach((api) => {
      const children = new Array<ModelNode>();
      this._collectOperations(api, children);
      this._collectParameters(api, children);
      this._collectSchemas(api, children);
      const node = ModelNode.apiNode(api, children);
      const splittedPath = this._splitPath(api.path);
      if (splittedPath.length == 1) {
        result.push(node);
      } else {
        this._getNode(splittedPath[1], nodeIdx, result).children!.push(node);
      }
    });
    return result;
  }

  private _getNode(
    path: string,
    nodeIdx: Map<string, ModelNode>,
    rootNodes: Array<ModelNode>
  ): ModelNode {
    if (nodeIdx.get(path)) return nodeIdx.get(path)!;
    const splittedPath = this._splitPath(path);
    const result = ModelNode.dirNode(splittedPath[0], path);
    if (splittedPath.length == 2) {
      this._getNode(splittedPath[1], nodeIdx, rootNodes).children!.push(result);
    } else {
      rootNodes.push(result);
    }
    nodeIdx.set(path, result);
    return result;
  }

  private _splitPath(path: string): Array<string> {
    const pathEndIdx = path.lastIndexOf('/');
    if (pathEndIdx < 0) return [path];
    return [path.substring(pathEndIdx + 1), path.substring(0, pathEndIdx)];
  }

  private _collectOperations(api: RestApiModel, result: Array<ModelNode>) {
    if (api.operations && api.operations.length > 0) {
      const children = new Array<ModelNode>();
      api.operations.forEach((o) => {
        const name = `${o.httpMethod}\u00A0${o.httpPath}`;
        children.push(new ModelNode('operation', name, o.path, o));
      });
      result.push(
        ModelNode.groupNode('operations', `${api.path}!operations`, children)
      );
    }
  }

  private _collectSchemas(api: RestApiModel, result: Array<ModelNode>) {
    if (api.schemas && api.schemas.length > 0) {
      const children = new Array<ModelNode>();
      api.schemas.forEach((s) =>
        children.push(new ModelNode('schema', s.code!, s.path!, s))
      );
      result.push(
        ModelNode.groupNode('models', `${api.path}!schemas`, children)
      );
    }
  }

  private _collectParameters(api: RestApiModel, result: Array<ModelNode>) {
    if (api.parameters && api.parameters.length > 0) {
      const children = new Array<ModelNode>();
      api.parameters.forEach((p) =>
        children.push(new ModelNode('parameter', p.name!, p.path!, p))
      );
      result.push(
        ModelNode.groupNode('parameters', `${api.path}!parameters`, children)
      );
    }
  }

  childrenAccessor = (node: ModelNode) =>
    node.path === '/' ? [] : node.children ?? [];
  expansionKey = (node: ModelNode) => node.path;
  hasChild = (_: number, node: ModelNode) => !!node.children;
  isRoot = (_: number, node: ModelNode) => node.path === '/';
  indent = '15';
}

class ApiTreeBuilder {
  result: Map<string, ModelNode> = new Map();
}
