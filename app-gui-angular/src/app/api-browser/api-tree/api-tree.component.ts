import { Component, OnInit, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTree, MatTreeModule } from '@angular/material/tree';
import { ApiBrowserService, ModelNode } from '../api-browser.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'sd-api-tree',
  standalone: true,
  imports: [MatTreeModule, MatButtonModule, MatIconModule],
  templateUrl: './api-tree.component.html',
  styleUrl: './api-tree.component.scss',
})
export class ApiTreeComponent implements OnInit {
  dataSource = new Array<ModelNode>();
  selected?: ModelNode;
  focused?: ModelNode;
  @ViewChild(MatTree) tree!: MatTree<ModelNode, string>;

  constructor(
    private readonly apiBrowserSerivce: ApiBrowserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.apiBrowserSerivce.getAllApis().then((models) => {
      this.dataSource = models;
      if (this.selected === undefined) {
        this.apiBrowserSerivce
          .getApiNode(this.route.snapshot.queryParamMap.get('path') ?? '/')
          .subscribe((node) => this.onSelect(node));
      }
    });
  }

  onSelect(node: ModelNode) {
    if (node === this.selected) return;
    this._expandNode(node);
    this.selected = node;
    this.router.navigate(['apis'], { queryParams: { path: node.path } });
  }

  private _expandNode(node: ModelNode) {
    if (!this.tree.isExpanded(node)) {
      if (node.parent) this._expandNode(node.parent);
      this.tree.toggle(node);
    }
  }

  onMouseEnter(node: ModelNode) {
    this.focused = node;
  }

  onMouseLeave() {
    this.focused = undefined;
  }

  refresh(node?: ModelNode) {
    console.log('api tree refresh: ' + node?.path);
    this.ngOnInit();
  }

  childrenAccessor = (node: ModelNode) =>
    node.path === '/' ? [] : node.children ?? [];
  expansionKey = (node: ModelNode) => node.path;
  hasChild = (_: number, node: ModelNode) => !!node.children;
  isRoot = (_: number, node: ModelNode) => node.path === '/';
  indent = '15';
}
