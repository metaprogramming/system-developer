import { Routes } from '@angular/router';
import { BrowserComponent } from './browser/browser.component';
import { ApiBrowserComponent } from './api-browser/api-browser.component';

export const routes: Routes = [
  { path: '', redirectTo: '/apis/', pathMatch: 'full' },
  { path: 'files', component: BrowserComponent },
  { path: 'apis', component: ApiBrowserComponent },
];
