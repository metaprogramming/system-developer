import { Component, OnInit, ViewChild } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { RouterOutlet } from '@angular/router';
import { ApiBrowserComponent } from './api-browser/api-browser.component';
import { BrowserComponent } from './browser/browser.component';

export type View = 'files' | 'apis' | 'apps' | 'docs';

@Component({
  selector: 'sd-root',
  standalone: true,
  imports: [
    RouterOutlet,
    ApiBrowserComponent,
    BrowserComponent,
    MatIconModule,
    MatListModule,
    MatSidenavModule, 
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'System Developer';
  view: View = 'apis';
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  ngOnInit(): void {
  }

  onResizeEnd() {
    setTimeout(() => {
      console.log('resize');
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }

}
