import { Component, OnInit, ViewChild } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { Router, RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'sd-root',
  standalone: true,
  imports: [
    RouterOutlet,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    RouterLink,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'System Developer';
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  constructor(public router: Router) {}

  ngOnInit(): void {}
}
